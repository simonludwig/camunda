#!/bin/sh

docker run --rm -i -e APPLICATION_DOMAIN=localhost -e GROUP_NAME=$1 -v $(pwd):/opt metal3d/mo docker-compose-instance.template > $1-docker-compose.yml

mkdir storage
mkdir storage/$1
mkdir storage/$1/code

cp -r code/ storage/$1/code # Das sind die Standard Projekte, die zur Verfügung stehen sollten
docker-compose up -d # Startet Traefik falls nicht bereits geschehen

docker-compose -f $1-docker-compose.yml up -d
