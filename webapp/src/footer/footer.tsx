import * as logo from "./footer.png";

export const Footer = () => {
    return <div style={{width: '100%'}}>
        <img style={{width: '100%'}} src={logo.default}/>
    </div>
}