import {Header} from "./header/header";
import {Content} from "./content";
import {Footer} from "./footer/footer";
import React, {useEffect, useState} from "react";
import {loadData, loadTasks} from "./ApplyStatus/Status";
import {Student} from "./Faker";
import {StudentForm} from "./form/StudentForm";
import ReactModal from "react-modal";
import {useModal} from "react-modal-hook";
import {ProcessDefinitionService, ProcessInstanceService} from "./generated";
import {myFetch} from "./Fetcher";
import {Modal} from "./modal";

export const EditPage = (props: any) => {
    const id = props.match.params.id;
    const [data, setData] = useState<Student | undefined>(undefined);
    useEffect(() => loadData(id).then(setData) as any, [])

    const [tasks, setTasks] = useState<[]>([]);
    useEffect(() => loadTasks(id).then(data => setTasks(data)) as any, [])

    const [showModal, hideModal] = useModal(() => (
        <ReactModal isOpen>
            <Modal>
                <h2>Ihre Daten wurden ge    speichert</h2>
                <button onClick={hideModal}>Schließen</button>
            </Modal>
        </ReactModal>
    ));

    return <div>
        <Header/>
        <Content>
            <div style={{background: 'lightyellow'}}>
                <h1>Verfolgen Sie Ihre Bewerbung</h1>
                {tasks.map((task: any) => {
                    return <a target={"_blank"} href={`/camunda/app/tasklist/default/#/?task=${task.id}`}>Aufgabe in öffnen</a>
                })}
                <StudentForm
                    onSubmit={(data) =>
                        myFetch(() => ProcessInstanceService.modifyProcessInstanceVariables(id, {modifications: data})
                            .then(() => showModal()))}
                    setData={setData} data={data}/>
            </div>
        </Content>
        <Footer/>
    </div>
}
