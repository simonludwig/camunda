import React from "react";

export const Modal = ({children}:{children: any}) => {
    return <div style={{
        color: 'white',
        background: "#b54e62",
        padding: '30px'
    }}>
        {children}
    </div>
}