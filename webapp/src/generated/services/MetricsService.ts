/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { MetricsIntervalResultDto } from '../models/MetricsIntervalResultDto';
import type { MetricsResultDto } from '../models/MetricsResultDto';
import { request as __request } from '../core/request';

export class MetricsService {

    /**
     * Retrieves a list of metrics, aggregated for a given interval.
     * @param name The name of the metric.
     * @param reporter The name of the reporter (host), on which the metrics was logged. This will have
     * value provided by the [hostname configuration property](https://docs.camunda.org/manual/7.14/reference/deployment-descriptors/tags/process-engine/#hostname).
     * @param startDate The start date (inclusive).
     * @param endDate The end date (exclusive).
     * @param firstResult Pagination of results. Specifies the index of the first result to return.
     * @param maxResults Pagination of results. Specifies the maximum number of results to return.
     * Will return less results if there are no more results left.
     * @param interval The interval for which the metrics should be aggregated. Time unit is seconds.
     * Default: The interval is set to 15 minutes (900 seconds).
     * @param aggregateByReporter Aggregate metrics by reporter.
     * @returns MetricsIntervalResultDto Request successful.
     * @throws ApiError
     */
    public static async interval(
        name?: 'activity-instance-start' | 'activity-instance-end' | 'job-acquisition-attempt' | 'job-acquired-success' | 'job-acquired-failure' | 'job-execution-rejected' | 'job-successful' | 'job-failed' | 'job-locked-exclusive' | 'executed-decision-elements' | 'history-cleanup-removed-process-instances' | 'history-cleanup-removed-case-instances' | 'history-cleanup-removed-decision-instances' | 'history-cleanup-removed-batch-operations',
        reporter?: string,
        startDate?: string,
        endDate?: string,
        firstResult?: number,
        maxResults?: number,
        interval: string = '900',
        aggregateByReporter?: string,
    ): Promise<Array<MetricsIntervalResultDto>> {
        const result = await __request({
            method: 'GET',
            path: `/metrics`,
            query: {
                'name': name,
                'reporter': reporter,
                'startDate': startDate,
                'endDate': endDate,
                'firstResult': firstResult,
                'maxResults': maxResults,
                'interval': interval,
                'aggregateByReporter': aggregateByReporter,
            },
            errors: {
                400: `Returned if some of the query parameters are invalid.`,
            },
        });
        return result.body;
    }

    /**
     * Retrieves the `sum` (count) for a given metric.
     * @param metricsName The name of the metric.
     * @param startDate The start date (inclusive).
     * @param endDate The end date (exclusive).
     * @returns MetricsResultDto Request successful.
     * @throws ApiError
     */
    public static async getMetrics(
        metricsName: 'activity-instance-start' | 'activity-instance-end' | 'job-acquisition-attempt' | 'job-acquired-success' | 'job-acquired-failure' | 'job-execution-rejected' | 'job-successful' | 'job-failed' | 'job-locked-exclusive' | 'executed-decision-elements' | 'history-cleanup-removed-process-instances' | 'history-cleanup-removed-case-instances' | 'history-cleanup-removed-decision-instances' | 'history-cleanup-removed-batch-operations',
        startDate?: string,
        endDate?: string,
    ): Promise<MetricsResultDto> {
        const result = await __request({
            method: 'GET',
            path: `/metrics/${metricsName}/sum`,
            query: {
                'startDate': startDate,
                'endDate': endDate,
            },
        });
        return result.body;
    }

}