/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { VersionDto } from '../models/VersionDto';
import { request as __request } from '../core/request';

export class VersionService {

    /**
     * Retrieves the version of the Rest API.
     * @returns VersionDto Request successful.
     * @throws ApiError
     */
    public static async getRestApiVersion(): Promise<VersionDto> {
        const result = await __request({
            method: 'GET',
            path: `/version`,
        });
        return result.body;
    }

}