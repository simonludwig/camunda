/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CountResultDto } from '../models/CountResultDto';
import type { ResourceOptionsDto } from '../models/ResourceOptionsDto';
import type { UserCredentialsDto } from '../models/UserCredentialsDto';
import type { UserDto } from '../models/UserDto';
import type { UserProfileDto } from '../models/UserProfileDto';
import { request as __request } from '../core/request';

export class UserService {

    /**
     * Get List
     * Query for a list of users using a list of parameters.
     * The size of the result set can be retrieved by using the Get User Count method.
     * [Get User Count](https://docs.camunda.org/manual/7.14/reference/rest/user/get-query-count/) method.
     * @param id Filter by user id
     * @param idIn Filter by a comma-separated list of user ids.
     * @param firstName Filter by the first name of the user. Exact match.
     * @param firstNameLike Filter by the first name that the parameter is a substring of.
     * @param lastName Filter by the last name of the user. Exact match.
     * @param lastNameLike Filter by the last name that the parameter is a substring of.
     * @param email Filter by the email of the user. Exact match.
     * @param emailLike Filter by the email that the parameter is a substring of.
     * @param memberOfGroup Filter for users which are members of the given group.
     * @param memberOfTenant Filter for users which are members of the given tenant.
     * @param potentialStarter Only select Users that are potential starter for the given process definition.
     * @param sortBy Sort the results lexicographically by a given criterion.
     * Must be used in conjunction with the sortOrder parameter.
     * @param sortOrder Sort the results in a given order. Values may be asc for ascending order or desc for descending order.
     * Must be used in conjunction with the sortBy parameter.
     * @param firstResult Pagination of results. Specifies the index of the first result to return.
     * @param maxResults Pagination of results. Specifies the maximum number of results to return.
     * Will return less results if there are no more results left.
     * @returns UserProfileDto Request successful.
     * @throws ApiError
     */
    public static async getUsers(
        id?: string,
        idIn?: string,
        firstName?: string,
        firstNameLike?: string,
        lastName?: string,
        lastNameLike?: string,
        email?: string,
        emailLike?: string,
        memberOfGroup?: string,
        memberOfTenant?: string,
        potentialStarter?: string,
        sortBy?: 'instanceId' | 'caseInstanceId' | 'dueDate' | 'executionId' | 'caseExecutionId' | 'assignee' | 'created' | 'description' | 'id' | 'name' | 'nameCaseInsensitive' | 'priority' | 'processVariable' | 'executionVariable' | 'taskVariable' | 'caseExecutionVariable' | 'caseInstanceVariable',
        sortOrder?: 'asc' | 'desc',
        firstResult?: number,
        maxResults?: number,
    ): Promise<Array<UserProfileDto>> {
        const result = await __request({
            method: 'GET',
            path: `/user`,
            query: {
                'id': id,
                'idIn': idIn,
                'firstName': firstName,
                'firstNameLike': firstNameLike,
                'lastName': lastName,
                'lastNameLike': lastNameLike,
                'email': email,
                'emailLike': emailLike,
                'memberOfGroup': memberOfGroup,
                'memberOfTenant': memberOfTenant,
                'potentialStarter': potentialStarter,
                'sortBy': sortBy,
                'sortOrder': sortOrder,
                'firstResult': firstResult,
                'maxResults': maxResults,
            },
            errors: {
                400: `Returned if some of the query parameters are invalid, for example if a \`sortOrder\` parameter is supplied,
                 * but no \`sortBy\`, or if an invalid operator for variable comparison is used. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Options
     * The `/user` resource supports two custom `OPTIONS` requests, one for the resource as such
     * and one for individual user instances. The `OPTIONS` request allows checking for the set of
     * available operations that the currently authenticated user can perform on the /user resource.
     * If the user can perform an operation or not may depend on various things, including the user's
     * authorizations to interact with this resource and the internal configuration of the process
     * engine.
     * @returns ResourceOptionsDto Request successful.
     * @throws ApiError
     */
    public static async availableOperations(): Promise<ResourceOptionsDto> {
        const result = await __request({
            method: 'OPTIONS',
            path: `/user`,
        });
        return result.body;
    }

    /**
     * Get List Count
     * Queries for the number of deployments that fulfill given parameters. Takes the same parameters as the
     * [Get Users](https://docs.camunda.org/manual/7.14/reference/rest/user/get-query/) method.
     * @param id Filter by user id
     * @param idIn Filter by a comma-separated list of user ids.
     * @param firstName Filter by the first name of the user. Exact match.
     * @param firstNameLike Filter by the first name that the parameter is a substring of.
     * @param lastName Filter by the last name of the user. Exact match.
     * @param lastNameLike Filter by the last name that the parameter is a substring of.
     * @param email Filter by the email of the user. Exact match.
     * @param emailLike Filter by the email that the parameter is a substring of.
     * @param memberOfGroup Filter for users which are members of the given group.
     * @param memberOfTenant Filter for users which are members of the given tenant.
     * @param potentialStarter Only select Users that are potential starter for the given process definition.
     * @returns CountResultDto Request successful.
     * @throws ApiError
     */
    public static async getUserCount(
        id?: string,
        idIn?: string,
        firstName?: string,
        firstNameLike?: string,
        lastName?: string,
        lastNameLike?: string,
        email?: string,
        emailLike?: string,
        memberOfGroup?: string,
        memberOfTenant?: string,
        potentialStarter?: string,
    ): Promise<CountResultDto> {
        const result = await __request({
            method: 'GET',
            path: `/user/count`,
            query: {
                'id': id,
                'idIn': idIn,
                'firstName': firstName,
                'firstNameLike': firstNameLike,
                'lastName': lastName,
                'lastNameLike': lastNameLike,
                'email': email,
                'emailLike': emailLike,
                'memberOfGroup': memberOfGroup,
                'memberOfTenant': memberOfTenant,
                'potentialStarter': potentialStarter,
            },
            errors: {
                400: `Returned if some of the query parameters are invalid, for example, if an invalid operator for variable
                 * comparison is used. See the [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Create
     * Create a new user.
     * @param requestBody
     * @returns any Request successful.
     * @throws ApiError
     */
    public static async createUser(
        requestBody?: UserDto,
    ): Promise<any> {
        const result = await __request({
            method: 'POST',
            path: `/user/create`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Delete
     * Deletes a user by id.
     * @param id The id of the user to be deleted.
     * @returns any Request successful.
     * @throws ApiError
     */
    public static async deleteUser(
        id: string,
    ): Promise<any> {
        const result = await __request({
            method: 'DELETE',
            path: `/user/${id}`,
            errors: {
                403: `Identity service is read-only (Cannot modify users / groups / memberships).`,
                404: `A Deployment with the provided id does not exist. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Options
     * The `/user` resource supports two custom `OPTIONS` requests, one for the resource as such
     * and one for individual user instances. The `OPTIONS` request allows checking for the set of
     * available operations that the currently authenticated user can perform on the /user resource.
     * If the user can perform an operation or not may depend on various things, including the user's
     * authorizations to interact with this resource and the internal configuration of the process
     * engine.
     * @param id The id of the user to be deleted.
     * @returns ResourceOptionsDto Request successful.
     * @throws ApiError
     */
    public static async availableUserOperations(
        id: string,
    ): Promise<ResourceOptionsDto> {
        const result = await __request({
            method: 'OPTIONS',
            path: `/user/${id}`,
        });
        return result.body;
    }

    /**
     * Update Credentials
     * Updates a user's credentials (password)
     * @param id The id of the user to be updated.
     * @param password The users new password.
     * @param authenticatedUserPassword The password of the authenticated user who changes the password of the user
     * (i.e., the user with passed id as path parameter).
     * @param requestBody
     * @returns any Request successful.
     * @throws ApiError
     */
    public static async updateCredentials(
        id: string,
        password: string,
        authenticatedUserPassword: string,
        requestBody?: UserCredentialsDto,
    ): Promise<any> {
        const result = await __request({
            method: 'PUT',
            path: `/user/${id}/credentials`,
            query: {
                'password': password,
                'authenticatedUserPassword': authenticatedUserPassword,
            },
            body: requestBody,
            errors: {
                400: `The authenticated user password does not match`,
                403: `Identity service is read-only (Cannot modify users / groups / memberships).`,
                404: `If the corresponding user cannot be found`,
                500: `The user could not be updated due to an internal server error. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Get Profile
     * Retrieves a user's profile.
     * @param id The id of the user to retrieve.
     * @returns UserProfileDto Request successful.
     * @throws ApiError
     */
    public static async getUserProfile(
        id: string,
    ): Promise<Array<UserProfileDto>> {
        const result = await __request({
            method: 'GET',
            path: `/user/${id}/profile`,
            errors: {
                404: `Execution with given id does not exist. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Unlock User
     * Unlocks a user by id.
     * @param id The id of the user to be unlocked.
     * @returns any Request successful.
     * @throws ApiError
     */
    public static async unlockUser(
        id: string,
    ): Promise<any> {
        const result = await __request({
            method: 'POST',
            path: `/user/${id}/unlock`,
            errors: {
                403: `The user who performed the operation is not a Camunda admin user.`,
                404: `User cannot be found. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

}