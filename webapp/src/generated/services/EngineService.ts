/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ProcessEngineDto } from '../models/ProcessEngineDto';
import { request as __request } from '../core/request';

export class EngineService {

    /**
     * Retrieves the names of all process engines available on your platform.
     * **Note**: You cannot prepend `/engine/{name}` to this method.
     * @returns ProcessEngineDto Request successful.
     * @throws ApiError
     */
    public static async getProcessEngineNames(): Promise<Array<ProcessEngineDto>> {
        const result = await __request({
            method: 'GET',
            path: `/engine`,
        });
        return result.body;
    }

}