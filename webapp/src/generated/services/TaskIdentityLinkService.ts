/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { IdentityLinkDto } from '../models/IdentityLinkDto';
import { request as __request } from '../core/request';

export class TaskIdentityLinkService {

    /**
     * Gets the identity links for a task by id, which are the users and groups that are in
     * *some* relation to it (including assignee and owner).
     * @param id The id of the task to retrieve the identity links for.
     * @param type Filter by the type of links to include.
     * @returns IdentityLinkDto Request successful.
     * @throws ApiError
     */
    public static async getIdentityLinks(
        id: string,
        type?: string,
    ): Promise<Array<IdentityLinkDto>> {
        const result = await __request({
            method: 'GET',
            path: `/task/${id}/identity-links`,
            query: {
                'type': type,
            },
            errors: {
                400: `Task with given id does not exist. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling) for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Adds an identity link to a task by id. Can be used to link any user or group to a task
     * and specify a relation.
     * @param id The id of the task to add a link to.
     * @param requestBody
     * @returns any Request successful.
     * @throws ApiError
     */
    public static async addIdentityLink(
        id: string,
        requestBody?: IdentityLinkDto,
    ): Promise<any> {
        const result = await __request({
            method: 'POST',
            path: `/task/${id}/identity-links`,
            body: requestBody,
            errors: {
                400: `Task with given id does not exist. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling) for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Removes an identity link from a task by id
     * @param id The id of the task to remove a link from.
     * @param requestBody
     * @returns any Request successful.
     * @throws ApiError
     */
    public static async deleteIdentityLink(
        id: string,
        requestBody?: IdentityLinkDto,
    ): Promise<any> {
        const result = await __request({
            method: 'POST',
            path: `/task/${id}/identity-links/delete`,
            body: requestBody,
            errors: {
                400: `Task with given id does not exist.
                 * See the [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling) for
                 * the error response format.`,
            },
        });
        return result.body;
    }

}