/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AttachmentDto } from '../models/AttachmentDto';
import { request as __request } from '../core/request';

export class TaskAttachmentService {

    /**
     * Gets the attachments for a task.
     * @param id The id of the task to retrieve the attachments for.
     * @returns AttachmentDto Request successful.
     * @throws ApiError
     */
    public static async getAttachments(
        id: string,
    ): Promise<Array<AttachmentDto>> {
        const result = await __request({
            method: 'GET',
            path: `/task/${id}/attachment`,
            errors: {
                404: `No task exists for the given task id. See the [Introduction](/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Creates an attachment for a task.
     * @param id The id of the task to add the attachment to.
     * @param requestBody
     * @returns AttachmentDto Request successful.
     * @throws ApiError
     */
    public static async addAttachment(
        id: string,
        requestBody?: any,
    ): Promise<AttachmentDto> {
        const result = await __request({
            method: 'POST',
            path: `/task/${id}/attachment/create`,
            body: requestBody,
            errors: {
                400: `The task does not exists or task id is null. No content or url to remote content exists. See the
                 * [Introduction](/reference/rest/overview/#error-handling) for the error response format.`,
                403: `The history of the engine is disabled. See the [Introduction](/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Removes an attachment from a task by id.
     * @param id The id of the task.
     * @param attachmentId The id of the attachment to be removed.
     * @returns any Request successful.
     * @throws ApiError
     */
    public static async deleteAttachment(
        id: string,
        attachmentId: string,
    ): Promise<any> {
        const result = await __request({
            method: 'DELETE',
            path: `/task/${id}/attachment/${attachmentId}`,
            errors: {
                403: `The history of the engine is disabled. See the [Introduction](/reference/rest/overview/#error-handling)
                 * for the error response format.`,
                404: `A Task Attachment for the given task id and attachment id does not exist. See the
                 * [Introduction](/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Retrieves a task attachment by task id and attachment id.
     * @param id The id of the task.
     * @param attachmentId The id of the attachment to be retrieved.
     * @returns AttachmentDto Request successful.
     * @throws ApiError
     */
    public static async getAttachment(
        id: string,
        attachmentId: string,
    ): Promise<AttachmentDto> {
        const result = await __request({
            method: 'GET',
            path: `/task/${id}/attachment/${attachmentId}`,
            errors: {
                404: `The attachment for the given task and attachment id does not exist or the history of the engine is
                 * disabled.
                 *
                 * See the [Introduction](/reference/rest/overview/#error-handling) for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Retrieves the binary content of a task attachment by task id and attachment id.
     * @param id The id of the task.
     * @param attachmentId The id of the attachment to be retrieved.
     * @returns string Request successful.
     * @throws ApiError
     */
    public static async getAttachmentData(
        id: string,
        attachmentId: string,
    ): Promise<string> {
        const result = await __request({
            method: 'GET',
            path: `/task/${id}/attachment/${attachmentId}/data`,
            errors: {
                404: `The attachment content for the given task id and attachment id does not exist, or the history of the
                 * engine is disabled.
                 *
                 * See the [Introduction](/reference/rest/overview/#error-handling) for the error response format.`,
            },
        });
        return result.body;
    }

}