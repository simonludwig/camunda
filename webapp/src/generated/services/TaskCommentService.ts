/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CommentDto } from '../models/CommentDto';
import { request as __request } from '../core/request';

export class TaskCommentService {

    /**
     * Gets the comments for a task by id.
     * @param id The id of the task to retrieve the comments for.
     * @returns CommentDto Request successful.
     * @throws ApiError
     */
    public static async getComments(
        id: string,
    ): Promise<Array<CommentDto>> {
        const result = await __request({
            method: 'GET',
            path: `/task/${id}/comment`,
            errors: {
                404: `No task exists for the given task id. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Creates a comment for a task by id.
     * @param id The id of the task to add the comment to.
     * @param requestBody **Note:** Only the `message` property will be used. Every other property passed to this endpoint will be ignored.
     * @returns CommentDto Request successful.
     * @throws ApiError
     */
    public static async createComment(
        id: string,
        requestBody?: CommentDto,
    ): Promise<CommentDto> {
        const result = await __request({
            method: 'POST',
            path: `/task/${id}/comment/create`,
            body: requestBody,
            errors: {
                400: `The task does not exist or no comment message was submitted. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
                403: `The history of the engine is disabled. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Retrieves a task comment by task id and comment id.
     * @param id The id of the task.
     * @param commentId The id of the comment to be retrieved.
     * @returns CommentDto Request successful.
     * @throws ApiError
     */
    public static async getComment(
        id: string,
        commentId: string,
    ): Promise<CommentDto> {
        const result = await __request({
            method: 'GET',
            path: `/task/${id}/comment/${commentId}`,
            errors: {
                404: `The task or comment with given task and comment id does not exist, or the history of
                 * the engine is disabled. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

}