/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { SchemaLogEntryDto } from '../models/SchemaLogEntryDto';
import type { SchemaLogQueryDto } from '../models/SchemaLogQueryDto';
import { request as __request } from '../core/request';

export class SchemaLogService {

    /**
     * Queries for schema log entries that fulfill given parameters.
     * @param version Only return schema log entries with a specific version.
     * @param firstResult Pagination of results. Specifies the index of the first result to return.
     * @param maxResults Pagination of results. Specifies the maximum number of results to return.
     * Will return less results if there are no more results left.
     * @returns SchemaLogEntryDto Request successful.
     * **Note**: In order to get any results a user of group `camunda-admin` must
     * be authenticated.
     * @throws ApiError
     */
    public static async getSchemaLog(
        version?: string,
        firstResult?: number,
        maxResults?: number,
    ): Promise<Array<SchemaLogEntryDto>> {
        const result = await __request({
            method: 'GET',
            path: `/schema/log`,
            query: {
                'version': version,
                'firstResult': firstResult,
                'maxResults': maxResults,
            },
        });
        return result.body;
    }

    /**
     * Queries for schema log entries that fulfill given parameters.
     * @param firstResult Pagination of results. Specifies the index of the first result to return.
     * @param maxResults Pagination of results. Specifies the maximum number of results to return.
     * Will return less results if there are no more results left.
     * @param requestBody
     * @returns SchemaLogEntryDto Request successful.
     * **Note**: In order to get any results a user of group camunda-admin must be
     * authenticated.
     * @throws ApiError
     */
    public static async querySchemaLog(
        firstResult?: number,
        maxResults?: number,
        requestBody?: SchemaLogQueryDto,
    ): Promise<Array<SchemaLogEntryDto>> {
        const result = await __request({
            method: 'POST',
            path: `/schema/log`,
            query: {
                'firstResult': firstResult,
                'maxResults': maxResults,
            },
            body: requestBody,
        });
        return result.body;
    }

}