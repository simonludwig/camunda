/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { PatchVariablesDto } from '../models/PatchVariablesDto';
import type { VariableValueDto } from '../models/VariableValueDto';
import { request as __request } from '../core/request';

export class TaskLocalVariableService {

    /**
     * Retrieves all variables of a given task by id.
     * @param id The id of the task to retrieve the variables from.
     * @param deserializeValues Determines whether serializable variable values (typically variables that store custom Java objects)
     * should be deserialized on the server side (default `true`).
     *
     * If set to `true`, a serializable variable will be deserialized on server side and transformed to JSON
     * using [Jackson's](https://github.com/FasterXML/jackson) POJO/bean property introspection feature.
     * Note that this requires the Java classes of the variable value to be on the REST API's classpath.
     *
     * If set to `false`, a serializable variable will be returned in its serialized format.
     * For example, a variable that is serialized as XML will be returned as a JSON string containing XML.
     *
     * **Note:** While `true` is the default value for reasons of backward compatibility, we recommend setting this
     * parameter to `false` when developing web applications that are independent of the Java process
     * applications deployed to the engine.
     * @returns VariableValueDto Request successful.
     * @throws ApiError
     */
    public static async getTaskLocalVariables(
        id: string,
        deserializeValues: boolean = true,
    ): Promise<Record<string, VariableValueDto>> {
        const result = await __request({
            method: 'GET',
            path: `/task/${id}/localVariables`,
            query: {
                'deserializeValues': deserializeValues,
            },
            errors: {
                500: `Task id is \`null\` or does not exist. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Updates or deletes the variables in the context of a task. Updates precede deletions. So, if a variable is
     * updated AND deleted, the deletion overrides the update.
     * @param id The id of the task to set variables for.
     * @param requestBody
     * @returns any Request successful.
     * @throws ApiError
     */
    public static async modifyTaskLocalVariables(
        id: string,
        requestBody?: PatchVariablesDto,
    ): Promise<any> {
        const result = await __request({
            method: 'POST',
            path: `/task/${id}/localVariables`,
            body: requestBody,
            errors: {
                400: `The variable value or type is invalid. For example the value could not be parsed to an \`Integer\` value
                 * or the passed variable type is not supported. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
                500: `Update or delete could not be executed because the task is \`null\` or does not exist.. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Removes a local variable from a task by id.
     * @param id The id of the task to delete the variable from.
     * @param varName The name of the variable to be removed.
     * @returns any Request successful.
     * @throws ApiError
     */
    public static async deleteTaskLocalVariable(
        id: string,
        varName: string,
    ): Promise<any> {
        const result = await __request({
            method: 'DELETE',
            path: `/task/${id}/localVariables/${varName}`,
            errors: {
                500: `Task id is \`null\` or does not exist. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Retrieves a variable from the context of a given task by id.
     * @param id The id of the task to retrieve the variable from.
     * @param varName The name of the variable to get
     * @param deserializeValue Determines whether serializable variable values (typically variables that store custom Java objects)
     * should be deserialized on the server side (default `true`).
     *
     * If set to `true`, a serializable variable will be deserialized on server side and transformed to JSON
     * using [Jackson's](https://github.com/FasterXML/jackson) POJO/bean property introspection feature.
     * Note that this requires the Java classes of the variable value to be on the REST API's classpath.
     *
     * If set to `false`, a serializable variable will be returned in its serialized format.
     * For example, a variable that is serialized as XML will be returned as a JSON string containing XML.
     *
     * Note: While `true` is the default value for reasons of backward compatibility, we recommend setting this
     * parameter to `false` when developing web applications that are independent of the Java process
     * applications deployed to the engine.
     * @returns VariableValueDto Request successful.
     * @throws ApiError
     */
    public static async getTaskLocalVariable(
        id: string,
        varName: string,
        deserializeValue: boolean = true,
    ): Promise<VariableValueDto> {
        const result = await __request({
            method: 'GET',
            path: `/task/${id}/localVariables/${varName}`,
            query: {
                'deserializeValue': deserializeValue,
            },
            errors: {
                404: `Variable with given id does not exist. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
                500: `Task id is \`null\` or does not exist. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Sets a variable in the context of a given task.
     * @param id The id of the task to set the variable for.
     * @param varName The name of the variable to set.
     * @param requestBody
     * @returns any Request successful.
     * @throws ApiError
     */
    public static async putTaskLocalVariable(
        id: string,
        varName: string,
        requestBody?: VariableValueDto,
    ): Promise<any> {
        const result = await __request({
            method: 'PUT',
            path: `/task/${id}/localVariables/${varName}`,
            body: requestBody,
            errors: {
                400: `The variable name, value or type is invalid, for example if the value could not be parsed to an \`Integer\`
                 * value or the passed variable type is not supported or a new transient variable has the name that is
                 * already persisted. See the [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
                500: `The variable name is \`null\`, or the Task id is \`null\` or does not exist. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Retrieves a binary variable from the context of a given task by id. Applicable for byte array and file
     * variables.
     * @param id The id of the task to retrieve the variable for.
     * @param varName The name of the variable to retrieve.
     * @returns string Request successful.
     * For binary variables or files without any MIME type information, a byte stream is returned.
     * File variables with MIME type information are returned as the saved type.
     * Additionally, for file variables the Content-Disposition header will be set.
     * @throws ApiError
     */
    public static async getTaskLocalVariableBinary(
        id: string,
        varName: string,
    ): Promise<string> {
        const result = await __request({
            method: 'GET',
            path: `/task/${id}/localVariables/${varName}/data`,
            errors: {
                400: `Variable with given id exists but is not a binary variable.See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
                404: `Variable with given id does not exist. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

    /**
     * Sets the serialized value for a binary variable or the binary value for a file variable.
     * @param id The id of the task to retrieve the variable for.
     * @param varName The name of the variable to retrieve.
     * @param requestBody For binary variables a multipart form submit with the following parts:
     * @returns any Request successful.
     * @throws ApiError
     */
    public static async setBinaryTaskLocalVariable(
        id: string,
        varName: string,
        requestBody?: any,
    ): Promise<any> {
        const result = await __request({
            method: 'POST',
            path: `/task/${id}/localVariables/${varName}/data`,
            body: requestBody,
            errors: {
                400: `The variable value or type is invalid, for example if no filename is set. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
                500: `Variable name is \`null\`, or the Task id is \`null\` or does not exist. See the
                 * [Introduction](https://docs.camunda.org/manual/7.14/reference/rest/overview/#error-handling)
                 * for the error response format.`,
            },
        });
        return result.body;
    }

}