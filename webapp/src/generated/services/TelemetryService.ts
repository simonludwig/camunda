/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { TelemetryConfigurationDto } from '../models/TelemetryConfigurationDto';
import { request as __request } from '../core/request';

export class TelemetryService {

    /**
     * Fetch Telemetry Configuration
     * Fetches Telemetry Configuration.
     * @returns TelemetryConfigurationDto Request successful.
     * @throws ApiError
     */
    public static async getTelemetryConfiguration(): Promise<TelemetryConfigurationDto> {
        const result = await __request({
            method: 'GET',
            path: `/telemetry/configuration`,
            errors: {
                401: `If the user who perform the operation is not a <b>camunda-admin</b> user.`,
            },
        });
        return result.body;
    }

    /**
     * Configure Telemetry
     * Configures whether Camunda receives data collection of the process engine setup and usage.
     * @param requestBody
     * @returns any Request successful.
     * @throws ApiError
     */
    public static async configureTelemetry(
        requestBody?: TelemetryConfigurationDto,
    ): Promise<any> {
        const result = await __request({
            method: 'POST',
            path: `/telemetry/configuration`,
            body: requestBody,
            errors: {
                401: `If the user who perform the operation is not a <b>camunda-admin</b> user.`,
            },
        });
        return result.body;
    }

}