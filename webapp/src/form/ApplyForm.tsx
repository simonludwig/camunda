import React, {useState} from "react";
import {ProcessDefinitionService} from "../generated";
import {myFetch} from "../Fetcher";
import ReactModal from "react-modal";
import {useModal} from "react-modal-hook";
import {StudentForm} from "./StudentForm";
import {FakerService, Student} from "../Faker";
import {Modal} from "../modal";

const FormStyle = {
    backgroundColor: "#405e9a",
    color: "white",
    padding: "20px"
}

export const ApplyForm = () => {

    const [data, setData] = useState<Student | undefined>(new FakerService().fakeStudent());

    const [showModal, hideModal] = useModal(() => (
        <ReactModal isOpen>
            <Modal>
                <p>Ihre Daten wurden gespeichert. Sie werden in kürze weitergeleitet</p>
            </Modal>
        </ReactModal>
    ));


    return <div style={FormStyle}><h1>Ihre Daten</h1>
        <StudentForm
            data={data}
            setData={setData}
            onSubmit={(student: Student) => {
                myFetch(() => ProcessDefinitionService.submitForm('key/thm-anmeldeverfahren'
                    , {variables: student as any})
                    .then((data) => {
                        showModal();
                        setTimeout(() => window.location.href = "/status/" + data.id,3000);
                    }))
            }}/>

    </div>
}