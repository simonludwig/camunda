import React from "react";
import Form from "@rjsf/core";

// @ts-ignore
export const StudentForm = ({data, setData,onSubmit}: { data: any, setData: (data: any) => void, onSubmit: (student: Student) => void }) => <Form schema={require('./form.json')}
                                                           onChange={(data: any) => {
                                                               setData(data.formData);
                                                           }}
                                                           formData={data}
                                                           onSubmit={(errors: any, values: any) => {
                                                               const payload: any = {}
                                                               Object.keys(data).forEach(key => {
                                                                   payload[key] = {
                                                                       name: key,
                                                                       "value": (data as any)[key] || window[key as any],
                                                                       "type": "String"
                                                                   }
                                                               });
                                                               onSubmit(payload);
                                                           }}
                                                           onError={(e:any) => alert(e)}/>