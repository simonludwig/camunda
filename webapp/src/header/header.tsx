import * as logo from "./header.png";
export const Header = () => {
    return <a href={"/"}>
        <div>
            <img
                style={{width: '100%'}}
                src={logo.default}/>
        </div>
    </a>
}