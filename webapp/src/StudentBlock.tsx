import {Student} from "./Faker";

const StudentStyle = {
    backgroundColor: "#b54e62",
    padding: "20px",
    color: 'white'
}

export const StudentBlock = ({id, student}: { id: string, student: Student }) => {
    return <div style={StudentStyle}>
        <h1>{student.vorname} {student.nachname}</h1>
        <p>{student.city} {student.postalcode}</p>
        <p>Versicherung: {student.insurance}</p>
        <p>{student.birthdate}</p>
        <p>{student.mail}</p>
        <p>{student.nationality}</p>
        <p>{student.hzb_file}</p>
        <a href={"/status/" + id}>Daten ändern</a>
        <a>In Camuna anzeigen</a>
    </div>
}