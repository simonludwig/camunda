var faker = require('faker');


export class FakerService {

    public fakeStudent = function (): Student {
        faker.locale = "de";
        return {
            vorname: faker.name.firstName(),
            nachname: faker.name.lastName(),
            birthdate: faker.date.past(20).toISOString(),
            insurance: faker.random.arrayElement(["IKK", "TKK"]),
            address: faker.address.streetAddress(),
            postalcode: faker.address.zipCode(),
            city: faker.address.city(),
            hzb_file: "https://res.cloudinary.com/hvioxpubt/image/upload/v1613130872/h7818pdliuewxgngvmm7.pdf",
            mail: faker.internet.email(),
            nationality: faker.address.country()
        };
    }
}

export interface Student {
    vorname: string,
    nachname: string,
    mail: string,
    insurance: string,
    hzb_file: string,
    birthdate: string,
    city: string,
    nationality: string,
    address: string,
    postalcode: string
}