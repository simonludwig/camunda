import {ReactNode} from "react";

const ContainerStyle = {
    maxWidth: "70vw",
    margin: '90px auto'
}

export const Content = ({children}:{children: ReactNode}) => {
    return <div style={ContainerStyle}>
        {children}
    </div>
}