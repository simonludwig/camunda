import {ProcessInstanceService, TaskService} from "../generated";
import React from "react";
import {Student} from "../Faker";

export const deserialize = (data: any) => {
    const newData: any = {};
    Object.keys(data).forEach(key => {
        newData[key] = data[key].value
    })
    return newData;
}

export const loadData = (id: string): Promise<Student> => ProcessInstanceService.getProcessInstanceVariables(id)
    .then(data => deserialize(data) as any).catch(e => alert(e))

export const loadTasks = (id: string): Promise<any> => TaskService.getTasks(id);
