export const myFetch = (fn: () => Promise<any>) => {
    fn().then((data) => {
        return data;
    }).catch((data: any) => {
        alert(data.message);
    })
}