import {Header} from "./header/header";
import {Content} from "./content";
import {Footer} from "./footer/footer";
import React from "react";
import {ApplyForm} from "./form/ApplyForm";

export const ApplyPage = () => {
    return <div>
        <Header/>
        <Content>
            <ApplyForm/>
        </Content>
        <Footer/>
    </div>
}