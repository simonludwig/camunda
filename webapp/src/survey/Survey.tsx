import React from "react";
import {Model, StylesManager, Survey} from "survey-react";
import {MessageService} from "../generated";
import {myFetch} from "../Fetcher";
import 'survey-react/modern.css';

const SurveyStyle = {
    background: '#f9d47f',
    padding: '20px'
}


StylesManager.applyTheme("modern");

export const MySurvey = ({processId}: { processId: string }) => {
    return <div style={SurveyStyle}>{<Survey
        onComplete={(data:any) => {
            const t =  Math.floor((Object.values(data.data).filter(val => val === "correct").length / Object.keys(data.data).length)*100);
            alert(t);
            myFetch(() => {
                return MessageService.deliverMessage({
                    processInstanceId: processId
                    , messageName: "Message_1ihgjn6",processVariables: {
                        "survey_result":{
                            "value": t,
                            "type": "Double"
                        }

                    }
                })
            });
        }}
        theme
        model={new Model(require('./questions.json'))}/>}
    </div>
};