import {Header} from "./header/header";
import {Content} from "./content";
import {loadData} from "./ApplyStatus/Status";
import {MySurvey} from "./survey/Survey";
import {Footer} from "./footer/footer";
import React, {useEffect, useState} from "react";
import {Student} from "./Faker";
import {StudentBlock} from "./StudentBlock";

const Grid = {
    display: 'grid',
    gridTemplateColumns: "1fr 3fr",
    gridGap: "30px"
}

export const SurveyPage = (props: any) => {
    const id = props.match.params.id;
    const [data, setData] = useState<Student | undefined>();
    useEffect(() => loadData(id).then(setData) as any, [])

    return <div>
        <Header/>
        <Content>
            <div style={Grid}>
                <div>{data && <StudentBlock id={id} student={data}/>}</div>
                <MySurvey processId={id}/>
            </div>
        </Content>
        <Footer/>
    </div>
}