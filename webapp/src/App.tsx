import React from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import {ApplyPage} from "./applyPage";
import {SurveyPage} from "./surveyPage";
import {EditPage} from "./editPage";


function App() {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path='/' component={ApplyPage}/>
                <Route path='/status/:id' component={EditPage}/>
                <Route path='/survey/:id' component={SurveyPage}/>
            </Switch>
        </BrowserRouter>
    );
}

export default App;
