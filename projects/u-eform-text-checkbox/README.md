# Übung "Textarea und Checkbox in Embedded Form"
Erstellen einer Embedded Form die eine Textarea und eine Checkbox enthält. Verknüpfen dieser Form mit einem User-Task. Wenn die Checkbox angehakt wird, soll an einer Verzweigung der Ast durchlaufen werden über den die Texteingaben aus der Form auf der Konsole ausgegeben werden. Falls die Checkbox nicht angehakt wird, soll der Prozess sofort zum Endereignis gehen.

## Voraussetzungen

1. Eclipse-Projekt `u-eform-text-checkbox` importieren
2. ggf. POM anpassen
3. Unvollständige Prozessmodelldatei `/u-eform-text-checkbox/src/main/resources/u-eform-text-checkbox.bpmn`liegt vor.

![u-eform-text-checkbox-screen1](camunda/storage/gruppe-4/u-eform-text-checkboxcheckbox/images/2018/09/u-eform-text-checkbox-screen1.png)

## Übung

1. Erstellen der Datei **expertenmeinung.html** im Ordner `/u-eform-text-checkbox/src/main/webapp/forms` für den User-Task "Expertenmeinung zu Spiel eingeben".

  Metadaten, Überschriften und Formlayout definieren.

  ```HTML

  <meta http-equiv="content-type" content="text/html; charset=utf-8">

  <h2>Experten Einschätzung vor dem Spiel</h2>
  <form role="form"
        name="variablesForm">
  <div class="row">
    <div class="col-xs-6">
  ```
  **Textarea** mit vier Zeilen zur Eingabe der Expertenmeinung erstellen und über die cam-Direktiven mit Prozessvariable `meinung` vom Typ `String` verknüpfen.

  ```HTML
  <!-- Textarea für Expertenmeinung -->
  <div class="form-group">
    <label for="inputAddress">Meinung</label>
    <div class="controls">
      <textarea class="form-control"
                cam-variable-name="meinung"
                cam-variable-type="String"
                rows="4"></textarea>
                <!-- Auf Groß- und Kleinschreibung des Datentyps "String" achten! -->
    </div>
  </div>
  ```
  Checkbox unter die Textarea einfügen. Die Checkbox über die Direktive `cam-variable-name` mit der Prozessvariablen send verknüpfen. Als Datentyp über `cam-variable-type` den Typ `Boolean` festlegen.
  Ergänzen Sie den nachfolgen HTML-Text um die zwei Direktiven analog zur obigen Textarea. Auf Schreibweise von **Boolean** achten.
  ```HTML
  <div class="control-group">
      <hr>
      <label class="control-label" >Soll die Einschätzung veröffentlicht werden?</label>
  <div class="controls">
    <input type="checkbox"

  ... TODO...

           class="form-control" />
  </div>
  </div>
  ```
  Abschnitte schließen und optional Javascript-Bereich hinzufügen.
  ```HTML
  </div>
  </div>

  <script cam-script type="text/form-script">
  // custom JavaScript goes here
  </script>
  </form>
  ```

2. In Prozessmodell den User-Task mit der HTML-Datei verknüpfen.

![u-eform-text-checkbox-screen5](camunda/storage/gruppe-4/u-eform-text-checkboxcheckbox/images/2018/09/u-eform-text-checkbox-screen5.png)

3. Alle Dateien und Änderungen speichern.

## Testen

Unit-Tests von Prozessmodellen mit User-Tasks werden in dieser Übung nicht behandelt.

## Projekt deployen (in Camunda-Engine spielen)

1. Projekt mit _F5-Taste_ refreshen
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven clean_ um Maven zurückzusetzen
3.  Rechtsklick auf Projektordner  _Maven-> Update Project ..._  dann Projekt anhaken (**Checkboxen unten beachten!**) und _OK_

![general-screen1](camunda/storage/gruppe-4/u-eform-text-checkboxcheckbox/images/2018/08/general-screen1.png)

4. Rechtsklick auf Projektordner und dann  _Run As -> Maven build..._
5. Im Konfigurationsfenster unter Goals `tomcat7:deploy` beim ersten deployen eingeben; ist das Projekt schon mal deployed worden, dann `tomcat7:redeploy` eingeben und mit _OK_ ausführen

## Prozess in camunda starten
1. Im Browser die Tasklist öffnen: `http://localhost:8080/camunda/app/tasklist/default/#/login`
2. Mit Admin einloggen (demo/demo)
3. Rechts oben "Start process" anklicken
4. Prozess im Fenster durch Anklicken auswählen
5. Im Folgefenster roten Knopf "Start" betätigen

## Prozess durchlaufen

1. Task "Expertenmeinung zum Spiel abgeben" unter "All Tasks" auswählen und **claimen**!

![2u-eform-text-checkbox-screen2](camunda/storage/gruppe-4/u-eform-text-checkboxcheckbox/images/2018/09/2u-eform-text-checkbox-screen2.png)

2. Expertenmeinung eingeben, Checkbox "veröffentlichen" anklicken und completen.

![u-eform-text-checkbox-screen3](camunda/storage/gruppe-4/u-eform-text-checkboxcheckbox/images/2018/09/u-eform-text-checkbox-screen3.png)

**Ergebnis auf Konsole:**
![u-eform-text-checkbox-screen4](camunda/storage/gruppe-4/u-eform-text-checkboxcheckbox/images/2018/09/u-eform-text-checkbox-screen4.png)

Da die Bool-Variable `send` über den Checkboxhaken auf true gesetzt wurde, wird der Ast im Prozessmodell durchlaufen der die Expertenmeinung über einen Skripttask auf der Konsole ausgibt.


## Doku/ weiterführende Quellen

[camunda-doku zu verschiedenen Controls in einer Embedded Form](https://docs.camunda.org/manual/7.9/reference/embedded-forms/controls/)

## Lösung

Die Lösung finden Sie im gleichnamigen eclipse-Projekt mit der Namensergänzung "-loesung"
