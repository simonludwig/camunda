# Übung "Einfache Service-Task Delegation"
Es soll ein Prozess erstellt werden mit einem Service-Task über den eine Java-Methode (in einer  Delegate-Klasse) ausgeführt wird die auf der Konsole eine Nachricht ausgibt.

## Voraussetzungen


1. Eclipse-Projekt `u-servicetask-delegation` importieren
2. ggf. POM anpassen

## Übung

1. Prozessmodell erstellen
  1. Prozessmodelldatei  `/u-servicetask-delegation/src/main/resources/u-servicetask-delegation.bpmn` mit Camunda-Modeler öffnen
  2. Prozesselemente erstellen

![u-servicetask-delegation](camunda/storage/gruppe-4/u-servicetask-delegationlegation/images/2018/07/u-servicetask-delegation.png)



  3. Service-Task erstellen und wichtige Attribute füllen (Attribut _Java Class_ erstmal leer lassen)

![u-servicetask-delegation-screen2](camunda/storage/gruppe-4/u-servicetask-delegationlegation/images/2018/07/u-servicetask-delegation-screen2.png)


2. Erstellen der Java-Delegate-Klasse
  1. In eclipse im Package `learnbpm.example.servicetaskdelegation` eine Klasse `NachrichtAusgebenDelegate.java` anlegen die das Interface `org.camunda.bpm.engine.delegate.JavaDelegate` implementiert.

![u-servicetask-delegation-screen3](camunda/storage/gruppe-4/u-servicetask-delegationlegation/images/2018/07/u-servicetask-delegation-screen3.png)

![u-servicetask-delegation-screen4](camunda/storage/gruppe-4/u-servicetask-delegationlegation/images/2018/07/u-servicetask-delegation-screen4.png)

  2. Generierte Klasse öffnen und in der Methode excute Code zur Nachrichtenausgabe erstellen

```java
public void execute(DelegateExecution execution) throws Exception {
		System.out.println("Ich werde mal ein Fussball-Tippspiel");
	}
```

3. In Prozessmodell im Service-Task die Java-Klasse mit Packageangabe hinterlegen (`learnbpm.example.servicetaskdelegation.NachrichtAusgebenDelegate`), die aufgerufen werden soll (**Achtung: Dateiname ohne .java**); am besten Package und Name aus eclipse rauskopieren

![u-servicetask-delegation-screen5](camunda/storage/gruppe-4/u-servicetask-delegationlegation/images/2018/07/u-servicetask-delegation-screen5.png)

4. Alle geänderten Dateien speichern

## Testen


### Projekt aktualisieren
1. Projekt mit _F5-Taste_ refreshen
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven clean_ um Maven zurückzusetzen
3.  Rechtsklick auf Projektordner  _Maven-> Update Project ..._  dann Projekt anhaken (**Checkboxen beachten! siehe unten**) und _OK_

![general-screen1](camunda/storage/gruppe-4/u-servicetask-delegationlegation/images/2018/07/general-screen1.png)

### Test starten

Rechtsklick auf Java-Datei `/u-servicetask-delegation/src/test/java/learnbpm/example/test/servicetaskdelegation/UnitTestProzess.java`  und dann _Run As -> JUnit Test_

Ergebnisse:

![u-servicetask-delegation-screen7](camunda/storage/gruppe-4/u-servicetask-delegationlegation/images/2018/07/u-servicetask-delegation-screen7.png)


**Info:**
Die Testmethoden `learnbpm.example.test.einfacherprozess.UnitTestUeinfacherProzess.testProzess()` und `learnbpm.example.test.einfacherprozess.UnitTestUeinfacherProzess.testTeilprozess()` wurden vorab erstellt. Eine andere Übung beschäftigt sich mit dem Erstellen von Tests.

## Projekt deployen (in Camunda-Engine spielen)

1. Projekt aktualisieren (siehe [Projekt aktualisieren](#projekt-aktualisieren))
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven build..._
3. Im Konfigurationsfenster unter Goals `tomcat7:deploy` beim ersten deployen eingeben; ist das Projekt schon mal deployed worden, dann `tomcat7:redeploy` eingeben und mit _OK_ ausführen


## Prozess in camunda starten
1. IM Browser die Tasklist öffnen: `http://localhost:8080/camunda/app/tasklist/default/#/login`
2. Mit Admin einloggen (demo/demo)
3. Rechts oben "Start process" anklicken
4. Prozess im Fenster durch Anklicken auswählen
5. Im Folgefenster roten Knopf "Start" betätigen


## Doku/ weiterführende Quellen



## Lösung
Die Lösung finden Sie im gleichnamigen eclipse-Projekt mit der Namensergänzung "-loesung"
