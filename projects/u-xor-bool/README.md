# Übung "XOR-Verzweigung anhand eines Wahrheitswertes"
Erstellen eines Prozesses mit einer XOR-Verzweigung bei der der die Verzweigungsbedingung eine Boolean-Variable ist. Der Einfachheit halber soll vor der Verzweigung über einen Script-Task eine Prozessvariable erzeugt und mit einem Wahrheitswert belegt werden. Anhand der Prozessvariable wird der zu durchlaufende Ast nach dem XOR-Gateway festgelegt. Je nach Ast werden unterschiedliche Nachrichten auf der Konsole ausgegeben.


## Voraussetzungen


1. Eclipse-Projekt `u-xor-bool` importieren
2. ggf. POM anpassen

## Übung

1. Prozessmodell erstellen
  1. Prozessmodelldatei  `/u-xor-bool/src/main/resources/u-xor-bool.bpmn` mit Camunda-Modeler öffnen
  2. Prozesselemente wie nachfolgend abgebildet erstellen

  ![u-xor-bool-screen1](camunda/storage/gruppe-4/u-xor-boolxor-bool/images/2018/07/u-xor-bool-screen1.png)


2. Im Skript-Task "Tippergebnis eingeben" den javascript-Code
  ```javascript
  execution.setVariable("tippergebnis",true);
  ```
einfügen durch den bei der Ausführung die execution-Umgebung der Prozessinstanz geholt, eine Prozessvariable "tippergebnis" erzeugt und mit dem Wert "true" belegt wird.

![u-xor-bool-screen2](camunda/storage/gruppe-4/u-xor-boolxor-bool/images/2018/07/u-xor-bool-screen2.png)

3. Verzweigungsbedingungen an den Ästen festlegen
  1. Für den Ast "falsch getippt" die Bedingungen als "Expression" erstellen. Der Ausdruck `${not tippergebnis}` prüft den Wert der Prozessvariablen "tippergebnis" und ist erfüllt wenn dieser false ist (sozusagen not true)

![u-xor-bool-screen4](camunda/storage/gruppe-4/u-xor-boolxor-bool/images/2018/07/u-xor-bool-screen4.png)

  2. Für den Ast "richtig getippt" analog verfahren. Der Ausdruck `${tippergebnis}` prüft den Wert der Prozessvariablen "tippergebnis" und ist erfüllt wenn dieser true ist

![u-xor-bool-screen3](camunda/storage/gruppe-4/u-xor-boolxor-bool/images/2018/07/u-xor-bool-screen3.png)

4. In den Skript-Tasks "Ausgabe" jeweils die Javascript-Methode
  ```javascript
print("eine Nachricht")
  ```
mit der passenden Nachricht je nach Verzweigungsast einfügen

![u-xor-bool-screen6](camunda/storage/gruppe-4/u-xor-boolxor-bool/images/2018/07/u-xor-bool-screen6.png)

![u-xor-bool-screen7](camunda/storage/gruppe-4/u-xor-boolxor-bool/images/2018/07/u-xor-bool-screen7.png)

5.  Dem Gateway die Id "gateway_tippergebnis" geben

![u-xor-bool-screen5](camunda/storage/gruppe-4/u-xor-boolxor-bool/images/2018/07/u-xor-bool-screen5.png)

6. Datei speichern

**Denkaufgabe: Warum benötigt das Gateway die Id "gateway_tippergebnis"? Was passiert wenn diese anders heißt?**

Probieren Sie es aus und recherchieren Sie im Code des Projekts

## Testen


### Projekt aktualisieren
1. Projekt mit _F5-Taste_ refreshen
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven clean_ um Maven zurückzusetzen
3.  Rechtsklick auf Projektordner  _Maven-> Update Project ..._  dann Projekt anhaken (**Checkboxen beachten! siehe unten**) und _OK_

![](camunda/storage/gruppe-4/u-xor-boolxor-bool/images/2018/07/general-screen1.png)

### Test starten

Rechtsklick auf Java-Datei `/u-xor-bool/src/test/java/learnbpm/example/test/xorbool/UnitTestProzess.java`  und dann _Run As -> JUnit Test_

Ergebnisse:

![u-xor-bool-screen8](camunda/storage/gruppe-4/u-xor-boolxor-bool/images/2018/07/u-xor-bool-screen8.png)

**Info:**
Die Testmethoden `learnbpm.example.test.einfacherprozess.UnitTestUeinfacherProzess.testProzess()` und `learnbpm.example.test.einfacherprozess.UnitTestUeinfacherProzess.testTeilprozess()` wurden vorab erstellt. Eine andere Übung beschäftigt sich mit dem Erstellen von Tests.

## Projekt deployen (in Camunda-Engine spielen)

1. Projekt aktualisieren (siehe [Projekt aktualisieren](#projekt-aktualisieren))
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven build..._
3. Im Konfigurationsfenster unter Goals `tomcat7:deploy` beim ersten deployen eingeben; ist das Projekt schon mal deployed worden, dann `tomcat7:redeploy` eingeben und mit _OK_ ausführen


## Prozess in camunda starten
1. Im Browser die Tasklist öffnen: `http://localhost:8080/camunda/app/tasklist/default/#/login`
2. Mit Admin einloggen (demo/demo)
3. Rechts oben "Start process" anklicken
4. Prozess im Fenster durch Anklicken auswählen
5. Im Folgefenster roten Knopf "Start" betätigen


## Doku/ weiterführende Quellen


## Lösung

Die Lösung finden Sie im gleichnamigen eclipse-Projekt mit der Namensergänzung "-loesung"
