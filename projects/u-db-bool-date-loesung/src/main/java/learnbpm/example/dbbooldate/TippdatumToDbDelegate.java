package learnbpm.example.dbbooldate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Date;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class TippdatumToDbDelegate implements JavaDelegate {

	public TippdatumToDbDelegate() {
	
	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {

		//Prozessvariablen aus Prozess holen
		Boolean bool_fristgerecht = (Boolean) execution.getVariable("fristgerecht");
		Date unformatiert_tippdatum = (Date) execution.getVariable("tippdatum");
		
		//Boolean zu int konvertieren da MySQL boolean nicht verarbeiten kann
				int fristgerecht = Boolean.valueOf(bool_fristgerecht).compareTo(Boolean.FALSE);
				
				
		//Datumsformat festlegen
				java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");		
				
	   //Prozessvariable in String im Format "yyy-MM-dd" umwandeln um in DB zu schreiben
				String tippdatum = sdf.format(unformatiert_tippdatum);		
		

				// Connection zu  DB aufbauen; DB-Name: tippspiel; user: root; password:
				// root
				Connection connection;

				//Datenbankname: tippspiel
				connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/tippspiel?user=root&password=root");

				Statement stmt = connection.createStatement();

				// Insert-Befehl als String erstellen
				// ACHTUNG: hier werden tippdatum und fristgerecht-Status über ein Update dem Tipp
				// mit der id 2 zugeordnet; das setzt voraus, dass dieser Eintrag schon in der DB vorhanden ist
				// sonst gibt es eine Fehlermeldung

				String sql = "UPDATE tippspiel.spieltipp SET tippdatum='"+tippdatum+"', fristgerecht='"+fristgerecht+"' WHERE idspieltipp='2'";

				// Optional: Ausgabe des Klassennamens und des SQL-Strings auf der Konsole zu Debuggingzwecken
				System.out.println(this.getClass().getName()+": \n"+sql);
				
				
				// Ausführen des Updates auf der DB
				stmt.executeUpdate(sql);
				
				// Datenbank schließen
				connection.close();
		
		
		
		
		
		
		

	}

}
