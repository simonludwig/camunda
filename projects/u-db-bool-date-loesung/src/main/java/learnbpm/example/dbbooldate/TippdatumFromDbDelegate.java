package learnbpm.example.dbbooldate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class TippdatumFromDbDelegate implements JavaDelegate {

	public TippdatumFromDbDelegate() {

		
	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {

		// Connection zu DB aufbauen; DB-Name: tippspiel; user: root; password: root
			Connection connection;

			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/tippspiel?user=root&password=root");

			Statement stmt = connection.createStatement();

			
			// ACHTUNG: hier wird das tippdatum für den Spieltipp mit der idspieltipp=3 aus der
			//Datenbank geholt. Das setzt voraus, dass es den Eintrag gibt sonst gibt es eine
			// Fehlermeldung
			String sql = "SELECT tippdatum FROM tippspiel.spieltipp WHERE idspieltipp=3";

			// Optional: Ausgabe des Klassennamens und des SQL-Strings auf der Konsole zu Debuggingzwecken
			System.out.println(this.getClass().getName()+": \n"+sql);
			
			
			// Ausführen des Selects auf der DB
			ResultSet res = stmt.executeQuery(sql);
			
			// Cursor des ResultSets auf die erste Position setzen
			res.first();
			
			// Wert aus erste Spalte über Methode getDate als Datum auslesen
			Date unformatiert_tippdatum2 = res.getDate(1);
				
			// Datumsformatierung festlegen
			SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
			
			// Formatierung auf die Datumsvariable anwenden und diese als String speichern
			String tippdatum2 = sdf.format(unformatiert_tippdatum2);  
			
			
			// Variable tippdatum2  als String in den Prozess geben
			execution.setVariable("tippdatum2", tippdatum2);

			// optional: Ausgabe des Klassennamens und des Tippdatums auf der Konsole zum Debugging
			String klassenname = this.getClass().getSimpleName();
			System.out.println("Ich bin das Tippdatum aus  "+klassenname+" : "+ tippdatum2);


	}

}
