package learnbpm.example.sendmail;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class SendEmailDelegate implements JavaDelegate {
	
	//ergänzte Parameter für E-Mailversand als statische Klassenvariablen
	private static final String HOST = "smtp.googlemail.com";
	private static final String USER = "christian.camunda@gmail.com";
	private static final String PWD = "camunda123";
	private static final Integer PORT = 465;
	

	public SendEmailDelegate() {

	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {

		//Prozessvariable holen
		String ergebnis = (String) execution.getVariable("spielergebnis");
	
		//Emailinstanz erzeugen
		Email email = new SimpleEmail();
		email.setCharset("utf-8");
		email.setHostName(HOST);
		email.setAuthentication(USER, PWD);
		email.setSmtpPort(PORT);
		email.setSSLOnConnect(true); 

		email.setFrom("noreply@learnbpm.org"); // Replyadresse angeben
		email.setSubject("Spielergebnis"); // Betreff
		//E-Mailtext mit integriertem Wert der Prozessvariablen
		email.setMsg("Lieber Tippteilnehmer\n Das Ergebnis des Spiels liegt vor:\n "+ ergebnis);

		//Empfänger setzen ( der Einfachheit halber ist hier der Sender auch der Empfänger)
		email.addTo("christian.camunda@gmail.com");

		email.send();
		
		
		

	}

}
