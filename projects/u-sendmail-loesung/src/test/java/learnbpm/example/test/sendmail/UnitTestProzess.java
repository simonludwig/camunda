package learnbpm.example.test.sendmail;

import org.apache.ibatis.logging.LogFactory;


import org.camunda.bpm.engine.runtime.ProcessInstance;

import org.camunda.bpm.engine.test.ProcessEngineRule;

import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;

import org.camunda.bpm.engine.test.Deployment;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineAssertions.processEngine;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*;

import java.text.SimpleDateFormat;


/**
 * Test case starting an in-memory database-backed Process Engine.
 */
public class UnitTestProzess {

	// Variante 1 (Standard): Testen eines einzigen Prozessmodells

	/*
	 * @ClassRule
	 * 
	 * @Rule public static ProcessEngineRule rule =
	 * TestCoverageProcessEngineRuleBuilder.create().build();
	 * 
	 */

	// Variante 2 wenn man mehr als ein Prozessmodell in einer Testklasse testen
	// will

	// @Rule
	// public ProcessEngineRule rule =
	// TestCoverageProcessEngineRuleBuilder.create().build();

	// hier Variante 1
	@ClassRule

	@Rule
	public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

	static {
		LogFactory.useSlf4jLogging(); // MyBatis
	}

	@Before
	public void setup() {
		init(rule.getProcessEngine());
	}

	// JUnit-Test für den gesamten Prozess
	@Test
	@Deployment(resources = "u-sendmail.bpmn") // hier die Prozessmodelle aufführen die getest werden sollen
	public void testProzess() {

		// Prozessinstanz des Prozessmodells mit dem Key (=Id des Prozessmodells)
		// erzeugen und starten

		ProcessInstance pi = processEngine().getRuntimeService().startProcessInstanceByKey("u-sendmail");
		
		//optional für Debugging
		System.out.println("Prozessinstanz mit der Id "+ pi.getId()+ " gestartet");
	}

	// JUnit-Test für einen Prozessteil
	@Test
	@Deployment(resources = "u-sendmail.bpmn")
	public void testTeilprozess() {

		
		// Prozessinstanz von "u-sendmail" vor dem Start der Aktivität mit der Id "ergebnisSendenId"
		// (der Service-Task "Spielergebnis versenden") starten
		// Beim Start die Prozessvariable "spielergebnis" erzeugen, mit Wert belegen
		// und in den Prozess reingeben
		// das ist notwendig, da die Variablen im Service-Task "Spielergebnis versenden" benötigt wird
	
		ProcessInstance pi = processEngine().getRuntimeService().createProcessInstanceByKey("u-sendmail")
		  .startBeforeActivity("ergebnisSendenId") 
		  .setVariable("spielergebnis","Deutschland-Brasilien 7:1")
		  .execute();
		
		//optional für Debugging
		System.out.println("Prozessinstanz mit der Id "+ pi.getId()+ " gestartet");

	}

}
