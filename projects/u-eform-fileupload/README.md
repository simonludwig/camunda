# Übung "Dateiupload aus Embedded Form und Ablage in Datenbank und Festplattenordner"
Über eine Embedded Form Datei in den Prozess als Prozessvariable hochladen und nachfolgend über einen Service-Task und eine implementierte JavaDelegate-Klasse diese Datei als BLOB in eine Datenbank schreiben und zusätzlich im Filesystem des Rechners ablegen.

## Voraussetzungen

1. Eclipse-Projekt `u-eform-fileupload` importieren
2. ggf. POM anpassen
3. mySQl (oder MariaDB) installiert und konfiguriert
4. Datenbank über das Skript `..\sqldump\u-eform-fileupload.sql` erzeugen
(benötigt wird die Tabelle `spielelisten` mit dem Attribut `listendatei` vom Typ `MEDIUMBLOB`)
5. Zugangsdaten für DB festlegen: user=root und password=root
6. Ordner `c:\test` im Filesystem angelegt
7. Beliebige PDF-Testdatei zum Hochladen vorhanden

**HINWEIS: AUS VEREINFACHUNGSZWECKEN GENÜGT DAS DB-SCHEMA NICHT DEN NORMALENFORMEN**

7. Prozessmodelldatei  `/u-eform-fileupload/src/main/resources/u-eform-fileupload.bpmn` liegt vor.

![u-eform-fileupload-screen9](camunda/storage/gruppe-4/u-eform-fileuploadleupload/images/2018/09/u-eform-fileupload-screen9.png)

## Übung

1. Erstellen der Datei **upload_spieleliste.html** im Ordner `/u-eform-fileupload/src/main/webapp/forms` für den User-Task "Spieleliste als Datei hochladen".

  Metadaten, Überschriften und Formlayout definieren.

    ```HTML
    <meta http-equiv="content-type" content="text/html; charset=utf-8">

    <h2>UPLOAD Spieleliste</h2>
      <form role="form"
        name="variablesForm">
      <div class="row">
        <div class="col-xs-6">
    ```

  Ein Input-Feld mit Dateiupload und folgenden Direktiven erzeugen: `cam-variable-name` (für den im Prozess verwendeten Dateinamen) , `cam-variable-type="File"` (zur Festlegung dass der Datentyp eine Datei ist) und `cam-max-filesize` (zum Festlegen der max. Dateigröße).

  ```HTML
  <div class="col-xs-6">

    <!-- Control zum Upload einer Datei -->

  <div class="control-group">
    <label class="control-label">PDF-Liste mit einer Spieleübersicht hochladen (max. 1 Datei, max. 2 MB) </label>
    <div class="controls">

  <input type="file"
       cam-variable-name="SPIELELISTE_DOC"
       cam-variable-type="File"
       cam-max-filesize="2000000" />
       </div>
    </div>
  </div>
  ```

  Abschnitte schließen und optional Javascript-Bereich hinzufügen.
    ```HTML
    </div>
    <script cam-script type="text/form-script">
    // custom JavaScript goes here
    </script>
    </form>
    ```

  HINWEIS: Im Prozessmodell ist der User-Task bereits mit der HTML-Datei expertenmeinung.html verknüpft.

2. Erzeugen der Java-Klasse `SpielelisteDateiToDbDelegate` im Package `learnbpm.example.eformfileupload`. Im Prozessmodell ist im Service-Task "Spieleliste in DB ablegen" dieser Klassenname bereits hinterlegt.

![u-eform-fileupload-screen10](camunda/storage/gruppe-4/u-eform-fileuploadleupload/images/2018/09/u-eform-fileupload-screen10.png)

3. In der Klasse `SpielelisteDateiToDbDelegate`  innerhalb der Methode `execute` die Logik implementieren um die hochgeladene Datei auszulesen und als BLOB in der Datenbank abzulegen. Außerdem soll die Datei im Filesystem im Ordner `c:\test` abgelegt werden.

 1. Schreiben der **Datei in die Datenbank**

    Holen der Datei als Prozessvariable
    ```java
    // Holen der Datei in der Prozessvariable SPIELELISTE_DOC
    FileValue spielelistedatei = execution.getVariableTyped("SPIELELISTE_DOC");
    ```
    Aufbau der Datenbankverbindung
    ```java
    // Connection zu DB aufbauen; DB-Name: tippspiel; user: root; password:
    		// root
    		Connection connection;
    		// Datenbankname: tippspiel
    		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/tippspiel?user=root&password=root");
    ```
    Auslesen der Datei in einen Stream, vorbereiten eines INSERT-Statements, schreiben des Streams als BLOB in das vorgefertigte Statement und ausführen des Statements.
    ```java
    // SQL-INSERT falls Spielelisten-Datei hochgeladen wurde
    		if (spielelistedatei != null) {

    			InputStream spielelistedateiInhalt = spielelistedatei.getValue(); // Byte-Sream der Datei
    																				// erzeugen
    			String fileName = spielelistedatei.getFilename(); // Dateiname ermitteln

    			// String mimeType = spielelistedatei.getMimeType(); // Mime-Type ermitteln
    			// (falls gebraucht)
    			// String encoding = spielelistedatei.getEncoding(); // Zeichencodierung
    			// ermitteln (falls gebraucht)

    			// Zum Testen den Namen der Spielelisten-Datei ausgeben
    			System.out.println("Ausgabe Dateinamen: " + fileName);

    			// INSERT erstellen der die Spielelisten-Datei in die DB schreibt
    			String sql = "INSERT INTO tippspiel.spielelisten (listendatei) VALUES (?)";

    			// vorgefertigtes SQL-Statement erstellen
    			PreparedStatement statement = connection.prepareStatement(sql);

    			// InputStream der Spielelisten-Datei als Blob dem vorgefertigten SQL-Statement
    			// hinzufügen
    			statement.setBlob(1, spielelistedateiInhalt);

    			// vorgefertiges Statement ausführen
    			statement.executeUpdate();
    ```
  2. Ablage der **Datei im Filesystem**

    Zurücksetzen des InputStreams der Datei und Füllen eines Byte-Arrays mit dem Stream.
    ```java
    // InputStream wieder an den Anfang setzen, da er für DB-Blob vorher schon ausgelesen wurde
    spielelistedateiInhalt.reset();
    // Byte-Array mit InputStream füllen
byte[] buffer = new byte[spielelistedateiInhalt.available()];
spielelistedateiInhalt.read(buffer);
    ```
    Ablagedatei erzeugen und die Inhalte des Byte-Arrays in Output-Stream schreiben der die erzeugte Ablagedatei füllt.

    ```java
       //Zufallszahl für Dateinamensanhängsel erzeugen
          Random r = new Random();
          Integer i = r.nextInt();
          String zufall = i.toString();

          String dateiUndPfad = "/test/testdatei_"+zufall+".pdf";

          //Datei erzeugen
          File zielDatei = new File(dateiUndPfad);
          //  Byte-Array von Inputstream in OutputStream schreiben und dann Outputstream schließen
          OutputStream outStream = new FileOutputStream(zielDatei);
          outStream.write(buffer);
    ```

    Alle Streams und Datenbank schließen.
    ```java
    outStream.close();
    spielelistedateiInhalt.close();
    }
    // Datenbank schließen
    connection.close();
    ```

4. Alle Dateien und Änderungen speichern.

## Testen

Unit-Tests von Prozessmodellen mit User-Tasks werden in dieser Übung nicht behandelt.

## Projekt deployen (in Camunda-Engine spielen)

1. Projekt mit _F5-Taste_ refreshen
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven clean_ um Maven zurückzusetzen
3.  Rechtsklick auf Projektordner  _Maven-> Update Project ..._  dann Projekt anhaken (**Checkboxen unten beachten!**) und _OK_

![general-screen1](camunda/storage/gruppe-4/u-eform-fileuploadleupload/images/2018/08/general-screen1.png)

4. Rechtsklick auf Projektordner und dann  _Run As -> Maven build..._
5. Im Konfigurationsfenster unter Goals `tomcat7:deploy` beim ersten deployen eingeben; ist das Projekt schon mal deployed worden, dann `tomcat7:redeploy` eingeben und mit _OK_ ausführen

## Prozess in camunda starten
1. Im Browser die Tasklist öffnen: `http://localhost:8080/camunda/app/tasklist/default/#/login`
2. Mit Admin einloggen (demo/demo)
3. Rechts oben "Start process" anklicken
4. Prozess im Fenster durch Anklicken auswählen
5. Im Folgefenster roten Knopf "Start" betätigen

## Prozess durchlaufen

1. Task "Spieleliste als Datei hochladen" unter "All Tasks" auswählen und **claimen**!

![u-eform-fileupload-screen1](camunda/storage/gruppe-4/u-eform-fileuploadleupload/images/2018/09/u-eform-fileupload-screen1.png)

2. Im Task beliebige PDF-Datei zum Testen auf dem Computer auswählen und Task completen.

![u-eform-fileupload-screen2](camunda/storage/gruppe-4/u-eform-fileuploadleupload/images/2018/09/u-eform-fileupload-screen2.png)

**Ergebnis in der Datenbank:**

![u-eform-fileupload-screen4](camunda/storage/gruppe-4/u-eform-fileuploadleupload/images/2018/09/u-eform-fileupload-screen4.png)

**Ergebnis der Dateiablage auf dem Rechner:**
![u-eform-fileupload-screen8](camunda/storage/gruppe-4/u-eform-fileuploadleupload/images/2018/09/u-eform-fileupload-screen8.png)

**Ergebnis auf Konsole:**

![u-eform-fileupload-screen5](camunda/storage/gruppe-4/u-eform-fileuploadleupload/images/2018/09/u-eform-fileupload-screen5.png)


## Doku/ weiterführende Quellen



## Lösung

Die Lösung finden Sie im gleichnamigen eclipse-Projekt mit der Namensergänzung "-loesung"
