package learnbpm.example.processvardelegate;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class ErgebnisseTippernameDelegate implements JavaDelegate {

	public ErgebnisseTippernameDelegate() {
		// TODO Auto-generated constructor stub
		

		
	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		// TODO Auto-generated method stub
		
		// auf die Prozessvariable mit dem Namen spieltipp zugreifen
		String teams = (String) execution.getVariable("teams");
		
		System.out.println(teams);
		

		// Prozessvariable spieltipp erzeugen, mit Wert belegen und in den Prozess zurückgeben
		execution.setVariable("spieltipp", "Tipp: 7:0");
		
		

	}

}
