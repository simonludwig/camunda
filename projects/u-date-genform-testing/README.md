# Übung "Testing des Projekts Generated Form für Datum und XOR-Gateway"
Erstellen von JUnit-Tests anhand eines vorhandenen Camunda-Projekts. Erstellen einer Testmethode die den Prozess startet und vollständig durchläuft. Simulieren von User-Tasks (starten, Belegung von Variablen mit Werten, "completen"), d.h., dass diese in der Testmethode automatisch durchgeführt werden.
Erstellen einer Testmethode mit der nur ein Teil des Prozesses durchgeführt wird. Start vor dem Gateway mit Übergabe notwendiger Prozessvariablen. Anzeigen der automatisch generierten grafischen Testabdeckung des Prozessmodells.

## Voraussetzungen

1. Eclipse-Projekt `u-date-genform-testing` importieren
2. ggf. POM anpassen

## Ausgangssituation

Das Prozessmodell ist wie unten abgebildet bereits vorhanden. Es enthält bereits alle Konfigurationen für zwei generated User-Forms, Verzweigungsbedingungen sowie zwei Skript-Tasks welche Ergebnisse auf der Konsole ausgeben.

![u-date-genform-testing-screen1](camunda/storage/gruppe-4/u-date-genform-testing-testing/images/2018/09/u-date-genform-testing-screen1.png)

Zur Erstellung von Testmethoden ist bereits die Klasse `UnitTestProzess.java` im Package `learnbpm.example.test.dategenform` angelegt. Die eingebundenen Testbiliotheken ermöglichen es JUnit-Testmethoden zu schreiben die auf einer camunda-Instanz durchgeführt werden die beim Testen in der InMemory-Datenbank H2 des Apache Tomcat läuft.


## Testmethoden erstellen

1. Erstellen Sie eine Testmethode `testCompleteProcessDategenform()` welche eine Prozessinstanz erzeugt und vom Start- bis zum Endereignis durchläuft.

```java
  // Methode zum Testen des gesamten Durchlauf des Prozesses vom Startereignis bis
	// zum Endereignis
	@Test
	@Deployment(resources = { "u-date-genform-testing.bpmn" }) // Einbindung des Prozessmodells in den Test
	public void testCompleteProcessDategenform() throws ParseException {

...

```

  1. Starten der Prozessinstanz über die Methode `startProcessInstanceByKey` der Testbibliothek und Übergabe der `Id` des Prozessmodells

  ```java
  // Prozess über dessen Key (=Id) starten ohne Prozessvariablen in de Prozess reinzugeben (hier nicht notwendig)
  ProcessInstance processInstance = processEngine().getRuntimeService()
      .startProcessInstanceByKey("u-date-genform-testing");
  ```

  2. "Simulieren" des User-Tasks "Tippfristende eingeben": Holen des Task über den `TaskService` (Zugriff auf alle Tasks der Prozess-Engine) durch eine `TaskQuery` mit Angabe des `taskName`. Erzeugen einer  Datumsprozessvariable, formatieren der Variable und Belegung mit einem Wert. Ablage der Variable in einer `Map` und Fertigstellung des Tasks über Aufruf der `complete`-Methode des `TaskService` mit Übergabe der `TaskId` und der `Map` in der die Variablen stecken die über den User-Task erfasst wurden.

  ```java

  // Nach dem User-Task mit dem Namen ... suchen

  			// Task-Service holen
  			TaskService taskService = processEngine().getTaskService();

  			// über verketteten Methodenaufruf (FLUENT-API) eine Query mit dem Task-Service
  			// starten
  			// die in diesem Fall nach Tasks mit dem Namen "Tippfristende eingeben" sucht und
  			// diese als typisierte Java-List zurückgibt
  			List<Task> Taskliste = taskService.createTaskQuery().taskName("Tippfristende eingeben").list();

  			// Erstes Element aus Taskliste holen
  			Task meinTask = Taskliste.get(0);

  			// Datumsvariable "tippfristende" die über den User-Task erfasst werden sollen
  			// erzeugen und mit Werten belegen

  			// erzeugen der Datumsvariable mit Wert und Festlegung des Formats
  			Date einTippfristende = new SimpleDateFormat("yyyy-MM-dd").parse("2017-10-11");

  			// Map erzeugen, in diese die Variable "tippfristende" mit Wert ablegen

  			Map<String, Object> variables = new HashMap<String, Object>();

  			variables.put("tippfristende", einTippfristende);

  			// Task "completen" und Prozessvariablen aus Map übergeben
  			taskService.complete(meinTask.getId(), variables);
 ```

   3. Simulieren des User-Taks "Tippdatum eingeben" analog zum User-Task "Tippfristende eingeben". **Achten Sie bei der Erstellung des Quelltexts auf Konsistenz der Variablennamen, Ids etc. mit denen des  Prozessmodells!**

```java
 // Nach dem User-Task mit dem Namen ... suchen

       // Task-Service holen
...TODO...

       // über verketteten Methodenaufruf (FLUENT-API) eine Query mit dem Task-Service
       // starten

...TODO...

       // Erstes Element aus Taskliste holen
...TODO...

       // erzeugen der Datumsvariable mit Wert und Festlegung des Formats
...TODO...

       // Map erzeugen, in diese die Variable "tippfristende" mit Wert ablegen
...TODO...

       // Task "completen" und Prozessvariablen aus Map übergeben
...TODO...
```

2. Erstellen Sie eine Testmethode `testProcessPartFromDategenform()`  welche eine Prozessinstanz erzeugt und die notwendigen Prozessvariablen übergibt und am Gateway die Prozessinstanz startet.

```java
// Methode zum Testen eines Teils des Prozesses (hinter dem Startereignis)
  // Einstiegspunkt in den Prozess muss definiert werden

... TODO...(Methodenkopf analog der Testmethode von oben)

```

Erzeugen (noch nicht starten) einer Prozessinstanz über die Methode `createProcessInstanceByKey` mit der Id des Prozessmodells. Angabe des Startpunktes über `startBeforeActivity` mit Übergabe der Id des Gateway-Elements. Dann über `setVariable` die Variablen definieren, belegen und an die Prozessinstanz übergeben. Abschließend über `execute` die Prozessinstanz starten.

```java
// Prozessinstanz von "u-date-genform-testing" vor dem Start der Aktivität mit der Id "gateway_tippergebnis"
// (das Gateway im Prozess) starten
// Beim Start die beiden Prozessvariablen "tippfristende" und "tippdatum" erzeugen, mit Werten belegen
// und in den Prozess reingeben
// das ist notwendig, da die Variablen in den Folgeaktivitäten (Gateway, Ausgabe) benötigt werden

processEngine().getRuntimeService().createProcessInstanceByKey("u-date-genform-testing")
  .startBeforeActivity("gateway_tippergebnis")
  .setVariable("tippfristende", new SimpleDateFormat("yyyy-MM-dd").parse("2017-09-01"))
  .setVariable("tippdatum", new SimpleDateFormat("yyyy-MM-dd").parse("2017-08-01"))
  .execute();
```

3. Datei speichern.

## Testen

### Projekt aktualisieren
1. Projekt mit _F5-Taste_ refreshen
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven clean_ um Maven zurückzusetzen
3.  Rechtsklick auf Projektordner  _Maven-> Update Project ..._  dann Projekt anhaken (**Checkboxen beachten! siehe unten**) und _OK_

![general-screen1](camunda/storage/gruppe-4/u-date-genform-testing-testing/images/2018/07/general-screen1.png)

### Tests durchführen und Testabdeckung anzeigen

Rechtsklick auf Java-Datei `/u-date-genform-testing/src/test/java/learnbpm/example/test/dategenform/UnitTestProzess.java` und dann _Run As -> JUnit Test_

**Ergebnisse Konsole:**

![u-date-genform-testing-screen2](camunda/storage/gruppe-4/u-date-genform-testing-testing/images/2018/09/u-date-genform-testing-screen2.png)


**Ergebnisfenster des  erfolgreichen JUnit-Tests:**

![u-date-genform-testing-screen3](camunda/storage/gruppe-4/u-date-genform-testing-testing/images/2018/09/u-date-genform-testing-screen3.png)


**Anzeigen der Testabdeckung:**

Die Testbibliothek erzeugt nach erfolgten JUnit-Tests automatisch ein Prozessbild mit eingefärbter Testeindeckung. Die HTML-Seiten finden sich im Projekt unter `/target/process-test-coverage/learnbpm.example.test.dategenform.UnitTestProzess`. Sie lassen sich über einen Rechtsklick und dann im Kontextmenü _Open With-> Web Browser_ anzeigen.

Screenshot `testCompleteProcessDategenform_u-date-genform-testing.html`:

![u-date-genform-testing-screen4](camunda/storage/gruppe-4/u-date-genform-testing-testing/images/2018/09/u-date-genform-testing-screen4.png)

Screenshot `testProcessPartFromDategenform_u-date-genform-testing.html`:

![u-date-genform-testing-screen5](camunda/storage/gruppe-4/u-date-genform-testing-testing/images/2018/09/u-date-genform-testing-screen5.png)

## Doku/ weiterführende Quellen

[Camunda-Doku zu Testabdeckung](https://github.com/camunda/camunda-bpm-process-test-coverage/)

[Camunda-Doku zu JUnit-Tests](https://docs.camunda.org/manual/7.9/user-guide/testing/)

[Modifizieren von Prozessinstanzen in JUnit-Test](https://docs.camunda.org/manual/7.8/user-guide/process-engine/process-instance-modification/#process-instance-modification-in-junit-tests)

[Starten von Prozessinstanzen](https://docs.camunda.org/manual/7.8/user-guide/process-engine/process-engine-concepts/#start-a-process-instance)

## Lösung

Die Lösung finden Sie im gleichnamigen eclipse-Projekt mit der Namensergänzung "-loesung"
