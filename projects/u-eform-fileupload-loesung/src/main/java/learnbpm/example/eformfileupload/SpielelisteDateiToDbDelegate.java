package learnbpm.example.eformfileupload;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.value.FileValue;

public class SpielelisteDateiToDbDelegate implements JavaDelegate {

	public SpielelisteDateiToDbDelegate() {

	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {

		// Holen der Datei in der Prozessvariable SPIELELISTE_DOC
		FileValue spielelistedatei = execution.getVariableTyped("SPIELELISTE_DOC");
		
		// Connection zu DB aufbauen; DB-Name: tippspiel; user: root; password:
		// root
		Connection connection;

		// Datenbankname: tippspiel
		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/tippspiel?user=root&password=root");



		// SQL-INSERT falls Spielelisten-Datei hochgeladen wurde
		if (spielelistedatei != null) {

			InputStream spielelistedateiInhalt = spielelistedatei.getValue(); // Byte-Sream der Datei
																				// erzeugen
			String fileName = spielelistedatei.getFilename(); // Dateiname ermitteln

			// String mimeType = spielelistedatei.getMimeType(); // Mime-Type ermitteln
			// (falls gebraucht)
			// String encoding = spielelistedatei.getEncoding(); // Zeichencodierung
			// ermitteln (falls gebraucht)

			// Zum Testen den Namen der Spielelisten-Datei ausgeben
			System.out.println("Ausgabe Dateinamen: " + fileName);

			// INSERT erstellen der die Spielelisten-Datei in die DB schreibt
			String sql = "INSERT INTO tippspiel.spielelisten (listendatei) VALUES (?)";

			// vorgefertigtes SQL-Statement erstellen
			PreparedStatement statement = connection.prepareStatement(sql);

			// InputStream der Spielelisten-Datei als Blob dem vorgefertigten SQL-Statement
			// hinzufügen
			statement.setBlob(1, spielelistedateiInhalt);

			// vorgefertiges Statement ausführen
			statement.executeUpdate();
			
			//-----------------------------------------------------
			
			// Zusätzlich/alternativ die Spielelisten-Datei  im Filessystem in einem Ordner ablegen
			// Voraussetzung: Auf dem Coputer existiert ein Verzeichnis c:\test !!
			
			//-----------------------------------------------------
			

			// InputStream wieder an den Anfang setzen, da er für DB-Blob vorher schon ausgelesen wurde
			spielelistedateiInhalt.reset();
			
	       // Byte-Array mit InputStream füllen
			 byte[] buffer = new byte[spielelistedateiInhalt.available()];
			 spielelistedateiInhalt.read(buffer);
			 
		//Zufallszahl für Dateinamensanhängsel erzeugen
			 Random r = new Random();
			 Integer i = r.nextInt();
			 String zufall = i.toString();
		
		
			 String dateiUndPfad = "/test/testdatei_"+zufall+".pdf";
			
			 //Datei erzeugen
			 File zielDatei = new File(dateiUndPfad);
			 //  Byte-Array von Inputstream in OutputStream schreiben und dann Outputstream schließen
			 OutputStream outStream = new FileOutputStream(zielDatei);
			 outStream.write(buffer);
			 outStream.close();
			 
			 spielelistedateiInhalt.close();
			
		}

		// Datenbank schließen
		connection.close();
		

	}

}
