# Übung "Select-Box in Embedded Form"
Anlegen einer Liste von Spielen über einen Service-Task und speichern in einer Prozessvariablen. Anschließend Erstellung einer Embedded Form mit Selectbox die mit der Spieleliste gefüllt werden soll. Nach Auswahl eines Spiels aus der Selectbox durch den Nutzer über einen User-Task soll abschließend das ausgewählte Spiel über einen Skript-Task auf der Konsole ausgegeben werden.

## Voraussetzungen

1. Eclipse-Projekt `u-eform-selectbox` importieren
2. ggf. POM anpassen
3. Unvollständige Prozessmodelldatei  `/u-eform-selectbox/src/main/resources/u-eform-text-checkbox.bpmn` liegt vor.

![u-eform-selectbox-screen4](camunda/storage/gruppe-4/u-eform-selectboxelectbox/images/2018/09/u-eform-selectbox-screen4.png)

## Übung

1. Erzeugen der Java-Klasse `SpielelisteErstellenDelegate` im Package `learnbpm.example.eformselectbox`. Im Prozessmodell ist im Service-Task "Spielliste erstellen" dieser Klassenname bereits hinterlegt.

![u-eform-selectbox-screen5](camunda/storage/gruppe-4/u-eform-selectboxelectbox/images/2018/09/u-eform-selectbox-screen5.png)

2. In der Klasse `SpielelisteErstellenDelegate`  innerhalb der Methode `execute` die Logik implementieren, dass eine Map `spieleliste` mit drei Beispieleinträgen zu Spielen erstellt wird und diese Map als Prozessvariable `ALLE_SPIELE` in den Prozess gegegeben wird. Die Inhalte der Prozessvariable sollen im JSON-Format gespeichert sein. Mit der Variable soll im nachfolgenden User-Task eine Selectbox gefüllt werden.

```java
  //Erstellen einer leeren Map
  		Map<String, String> spieleliste = new HashMap<String, String>();

  		// Einträge für die Selectbox in Map anlegen
  		// erster Parameter der put-Methode (key der Map) wird in Selectbox als Wert hinterlegt
  		// zweiter Parameter der put-Methode (value der Map) wird in Selectbox als Texteintrag angezeigt
  		// Beispiel: Eintrag in Selectbox "Australien-England"; Wert "AUS-ENG"
  		//der ausgewählte Wert kann über die Embedded Form als Prozessvariable weitergegeben werden
  		spieleliste.put("AUS-ENG", "Australien-England");
  		spieleliste.put("GER-FR", "Deutschland-Frankreich");
  		spieleliste.put("ESP-IT", "Spanien-Italien");


  		// Erzeugen einer Prozessvariablen "ALLE_SPIELE" die als Werte die Map mit den Spieleinträgen übergeben bekommt
  		// beim Erzeugen werden die Inhalte der Prozessvariablen im JSON-Format abgelegt
  		// dies ist notwendig um innerhalb einer Embedded Form über eine Direktive die Einträge
  		// für eine Selectbox nutzen zu können
  		execution.setVariable("ALLE_SPIELE",Variables.objectValue(spieleliste)
  				.serializationDataFormat(SerializationDataFormats.JSON)
  				.create());
  ```

3. Erstellen der Datei **spielauswahl.html** im Ordner `/u-eform-selectbox/src/main/webapp/forms` für den User-Task "Spiel auswählen".

  Metadaten, Überschriften und Formlayout definieren.
    ```HTML
    <meta http-equiv="content-type" content="text/html; charset=utf-8">

    <h2>Spiel auswählen</h2>
    <form class="form-horizontal">
    ```

  Select-Control mit dem Label "Spiele" definieren der über die Direktive `cam-choices` die Einträge der Prozessvariable `ALLE_SPIELE` für die Befüllung der Selectbox nutzt. Außerdem über `cam-variable-name` definieren, dass der vom Nutzer ausgewählte Selectbox-Eintrag in der Prozessvariable `spiel` abgelegt wird. Der Typ der Variable muss über `cam-variable-type` mit angegeben werden, da die Variable neu erzeugt werden soll (vorher noch nicht existiert).

    ```HTML
    <div class="control-group">

       <label class="control-label">Spiele</label>
       <div class="controls">

           <select cam-variable-name="spiel"
           cam-variable-type="String"
           cam-choices="ALLE_SPIELE">
   </select>
         </div>
     </div>
    ```
  Abschnitte schließen und optional Javascript-Bereich hinzufügen.
    ```HTML
    <script cam-script type="text/form-script">
    // custom JavaScript goes here
    </script>
    </form>
    ```

4. Verknüpfung zwischen User-Task "Spiel auswählen" und Embedded Form "spielauswahl.html" im Prozessmodell herstellen.

  ![u-eform-selectbox-screen6](camunda/storage/gruppe-4/u-eform-selectboxelectbox/images/2018/09/u-eform-selectbox-screen6.png)

5. Im Skript-Task "Spiel ausgeben"  die Prozessvariable `spiel` auf der Konsole ausgeben. Diese Variable wurde im vorangegangenen User-Task erzeugt und enthält den über die Selectbox ausgewählten Wert.

  ![u-eform-selectbox-screen7](camunda/storage/gruppe-4/u-eform-selectboxelectbox/images/2018/09/u-eform-selectbox-screen7.png)


6. Alle Dateien und Änderungen speichern.

## Testen

Unit-Tests von Prozessmodellen mit User-Tasks werden in dieser Übung nicht behandelt.

## Projekt deployen (in Camunda-Engine spielen)

1. Projekt mit _F5-Taste_ refreshen
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven clean_ um Maven zurückzusetzen
3.  Rechtsklick auf Projektordner  _Maven-> Update Project ..._  dann Projekt anhaken (**Checkboxen unten beachten!**) und _OK_

![general-screen1](camunda/storage/gruppe-4/u-eform-selectboxelectbox/images/2018/08/general-screen1.png)

4. Rechtsklick auf Projektordner und dann  _Run As -> Maven build..._
5. Im Konfigurationsfenster unter Goals `tomcat7:deploy` beim ersten deployen eingeben; ist das Projekt schon mal deployed worden, dann `tomcat7:redeploy` eingeben und mit _OK_ ausführen

## Prozess in camunda starten
1. Im Browser die Tasklist öffnen: `http://localhost:8080/camunda/app/tasklist/default/#/login`
2. Mit Admin einloggen (demo/demo)
3. Rechts oben "Start process" anklicken
4. Prozess im Fenster durch Anklicken auswählen
5. Im Folgefenster roten Knopf "Start" betätigen

## Prozess durchlaufen

1. Task "Spiel auswählen" unter "All Tasks" auswählen und **claimen**!

![u-eform-selectbox-screen1](camunda/storage/gruppe-4/u-eform-selectboxelectbox/images/2018/09/u-eform-selectbox-screen1.png)

2. In dem Task über die Selectbox einen Eintrag auswählen und dann completen.

![u-eform-selectbox-screen2](camunda/storage/gruppe-4/u-eform-selectboxelectbox/images/2018/09/u-eform-selectbox-screen2.png)

**Ergebnis auf Konsole:**

![u-eform-selectbox-screen8](camunda/storage/gruppe-4/u-eform-selectboxelectbox/images/2018/09/u-eform-selectbox-screen8.png)

## Doku/ weiterführende Quellen

[camunda-doku zu Select-Boxen](https://docs.camunda.org/manual/7.8/reference/embedded-forms/controls/select/)

## Lösung

Die Lösung finden Sie im gleichnamigen eclipse-Projekt mit der Namensergänzung "-loesung"
