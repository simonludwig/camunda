package learnbpm.example.processstartmessage;

import java.util.HashMap;
import java.util.Map;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.runtime.ProcessInstance;

public class StartProcessTippausgabeDelegate implements JavaDelegate {

	public StartProcessTippausgabeDelegate() {
	
	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		
		// Prozessvariable holen
		String meinSpieltipp = (String) execution.getVariable("spieltipp");
		
		//Map erstellen und Prozessvariable in Map ablegen
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("spieltipp", meinSpieltipp);
		
		// RuntimeService der Engine holen um auf Prozesse zuzugreifen
		RuntimeService rtm = execution.getProcessEngineServices().getRuntimeService();
			
	    // Message mit dem dem Namen "SpieltippVorhanden" an die Engine geben um Instanz
		// des Prozessmodells zu starten, das ein Message-Start-Event für den Messagenamen "SpieltippVorhanden"
		// definiert hat.
		rtm.startProcessInstanceByMessage("SpieltippVorhanden",variables);
		
		// Wichtig, die Map nach Messageerzeugung wieder zu leeren! Vor allem wenn mehrere Prozessinstanzen
		// hintereinander mit Messages gestartet werden sollen.
		variables.clear();
		
		

	}

}
