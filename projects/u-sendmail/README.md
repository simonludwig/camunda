# Übung "E-Mail aus Prozess heraus generieren und senden"

Prozess mit einem Skript-Task über den ein Spielergebnis als Prozessvariable erstellt wird und nachfolgend über einen Send-Task eine E-Mail mit dem Spielergebnis erstellt und automatisch versendet wird.




## Voraussetzungen


1. Eclipse-Projekt `u-sendmail` importieren
2. ggf. POM anpassen

## Übung

1. Prozessmodell erstellen
  1. Prozessmodelldatei `/u-sendmail/src/main/resources/u-sendmail.bpmn`  mit Camunda-Modeler öffnen
  2. Prozesselemente erstellen

    ![u-sendmail-screen4](camunda/storage/gruppe-4/u-sendmailsendmail/images/2018/09/u-sendmail-screen4.png)

2. Skript-Task "Spielergebnis erstellen" um Skriptcode ergänzen der die Prozessvariable `spielergebnis` erstellt und mit dem Text "Portugal-Griechenland 1:1" belegt.

![u-sendmail-screen5](camunda/storage/gruppe-4/u-sendmailsendmail/images/2018/09/u-sendmail-screen5.png)

3. Java-Delegate-Klasse für Send-Task "Spielergebnis versenden" erstellen. In dieser Klasse soll die Prozessvariable `spielergebnis` geholt werden, eine Textmail mit eingebautem Spielergebnis erstellt und per SSL über das SMTP-Protokoll an eine Test-Emailadresse gesendet werden.

  Klasse `SendEmailDelegate.java` im Package `learnbpm.example.sendmail` anlegen und das Interface `JavaDelegate` implementieren (siehe auch Übung _u-servicetask-delegation_)

  Notwendige Parameter für den Emailversand als statische Klassenvariablen einfügen
  ```java
  private static final String HOST = "smtp.googlemail.com";
  private static final String USER = "christian.camunda@gmail.com";
  private static final String PWD = "camunda123";
  private static final Integer PORT = 465;
  ```
  In der `execute`-Methode nachfolgende Logik implementieren.

  Prozessvariable `spielergebnis` holen.
  ```java
  // .. TODO ..
  ```
  Email erstellen und versenden.
  ```java
  //Emailinstanz erzeugen
  Email email = new SimpleEmail();

  //verschiedene Parameter zur Versendung setzen
  email.setCharset("utf-8");
  email.setHostName(HOST);
  email.setAuthentication(USER, PWD);
  email.setSmtpPort(PORT);
  email.setSSLOnConnect(true); 

  email.setFrom("noreply@learnbpm.org"); // Replyadresse angeben
  email.setSubject("Spielergebnis"); //Betreff

  // .. TODO .. mit der Methode org.apache.commons.mail.Email.setMsg(String arg0)
  // der email den folgenden Text anfügen der auch das Spielergebnis enthält.

  //Lieber Tippteilnehmer
   //Das Ergebnis des Spiels liegt vor:
  // HIER DAS SPIELERGEBNIS EINFÜGEN

  //Empfänger setzen ( der Einfachheit halber ist hier der Sender auch der Empfänger)
  email.addTo("christian.camunda@gmail.com");

  email.send();
  ```

4. Im Prozessmodell den Send-Task mit der Klasse  `SendEmailDelegate.java` verknüpfen.

  ![u-sendmail-screen6](camunda/storage/gruppe-4/u-sendmailsendmail/images/2018/09/u-sendmail-screen6.png)

5. Alle geänderten Dateien speichern.

## Testen

### Projekt aktualisieren
1. Projekt mit _F5-Taste_ refreshen
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven clean_ um Maven zurückzusetzen
3.  Rechtsklick auf Projektordner  _Maven-> Update Project ..._  dann Projekt anhaken (**Checkboxen beachten! siehe unten**) und _OK_

![general-screen1](camunda/storage/gruppe-4/u-sendmailsendmail/images/2018/07/general-screen1.png)

### Test starten

Rechtsklick auf Java-Datei `learnbpm.example.test.sendmail.UnitTestProzess`  und dann _Run As -> JUnit Test_

**Ergebnisse Email-Postfach**

![u-sendmail-screen8](camunda/storage/gruppe-4/u-sendmailsendmail/images/2018/09/u-sendmail-screen8.png)
![u-sendmail-screen2](camunda/storage/gruppe-4/u-sendmailsendmail/images/2018/09/u-sendmail-screen2.png)

**VERSTÄNDNISFRAGE**: Wieso wurden zwei E-Mails versendet und wieso enthält die zweite E-Mail das Spieleregbnis _Deutschland-Brasilien 7:1_?


Ergebnisfenster des JUnit-Tests:

![u-sendmail-screen7](camunda/storage/gruppe-4/u-sendmailsendmail/images/2018/09/u-sendmail-screen7.png)

**Info:**
Die Testmethoden `testProzess` und `testTeilprozess` wurden vorab erstellt. Eine andere Übung beschäftigt sich mit dem Erstellen von Tests.

## Projekt deployen (in Camunda-Engine spielen)

1. Projekt aktualisieren (siehe [Projekt aktualisieren](#projekt-aktualisieren))
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven build..._
3. Im Konfigurationsfenster unter Goals `tomcat7:deploy` beim ersten deployen eingeben; ist das Projekt schon mal deployed worden, dann `tomcat7:redeploy` eingeben und mit _OK_ ausführen


## Prozess in camunda starten
1. Im Browser die Tasklist öffnen: `http://localhost:8080/camunda/app/tasklist/default/#/login`
2. Mit Admin einloggen (demo/demo)
3. Rechts oben "Start process" anklicken
4. Prozess im Fenster durch Anklicken auswählen
5. Im Folgefenster roten Knopf "Start" betätigen


## Doku/ weiterführende Quellen

## Lösung

Die Lösung finden Sie im gleichnamigen eclipse-Projekt mit der Namensergänzung "-loesung"
