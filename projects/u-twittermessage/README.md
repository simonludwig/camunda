# Übung "Twitternachricht (Tweet) aus Prozess heraus generieren und senden"
Prozess mit einem Skript-Task über den ein Spielergebnis als Prozessvariable erstellt wird und nachfolgend über einen Send-Task ein Tweet (Twitternachricht) mit dem Spielergebnis erstellt und automatisch versendet wird.

## Voraussetzungen

1. Eclipse-Projekt `u-twittermessage` importieren
2. ggf. POM anpassen

## Übung

1. Prozessmodell erstellen
  1. Prozessmodelldatei `/u-twittermessage/src/main/resources/u-twittermessage.bpmn`  mit Camunda-Modeler öffnen
  2. Prozesselemente erstellen

![u-twittermessage-screen1](camunda/storage/gruppe-4/u-twittermessagermessage/images/2018/09/u-twittermessage-screen1.png)

2. Skript-Task "Spielergebnis erstellen" um Skriptcode ergänzen der die Prozessvariable `spielergebnis` erstellt und mit dem Text "Portugal-Griechenland 1:1" belegt.

![u-sendmail-screen5](camunda/storage/gruppe-4/u-twittermessagermessage/images/2018/09/u-sendmail-screen5.png)

3. Java-Delegate-Klasse für Send-Task "Spielergebnis als Tweet versenden" erstellen. In dieser Klasse soll die Prozessvariable `spielergebnis` geholt werden, eine Twitterverbindung eingerichtet werden und das Spielergebnis als Tweet übermittelt werden

  Klasse `SendeTweetDelegate.java` im Package `learnbpm.example.twittermessage` anlegen und das Interface `JavaDelegate` implementieren (siehe auch Übung _u-servicetask-delegation_)

  Notwendige Twitterbibliotheken importieren.
  ```java
  import twitter4j.Twitter;
  import twitter4j.TwitterFactory;
  import twitter4j.auth.AccessToken;
  ```

    In der `execute`-Methode nachfolgende Logik implementieren.

    Prozessvariable `spielergebnis` holen.
    ```java
    // .. TODO ..
    ```

  Twitterverbindung einrichten, Tweet mit Spielergebnis erstellen und senden.

  ```java
  // Hilfsmethoden zum Testen um an jeden Tweet Zufallszahl zu hängen um ihn zu unterscheiden
  // Grund: Twitter meldet Fehler wenn gleicher Tweet zweimal gesendet wird
  Random r = new Random();
  Integer zufallszahl = r.nextInt();

  //Tweetnachricht zusammenstellen
  String content = zufallszahl.toString()+ " Das Spielergebnis lautet "+ ergebnis;

  // Zugangstoken des Camunda-Twittertestaccounts holen
  AccessToken accessToken = new AccessToken("220324559-jet1dkzhSOeDWdaclI48z5txJRFLCnLOK45qStvo", "B28Ze8VDucBdiE38aVQqTxOyPc7eHunxBVv7XgGim4say");

  //Twitternachricht instanzieren
  Twitter twittermsg = new TwitterFactory().getInstance();

  //Authentifizierung bei Twitter
  twittermsg.setOAuthConsumer("lRhS80iIXXQtm6LM03awjvrvk", "gabtxwW8lnSL9yQUNdzAfgBOgIMSRqh7MegQs79GlKVWF36qLS");
  twittermsg.setOAuthAccessToken(accessToken);

  //Senden der Nachricht
  twittermsg.updateStatus(content);
  ```

4. Im Prozessmodell den Send-Task mit der Klasse `SendeTweetDelegate.java ` verknüpfen.

![u-twittermessage-screen2](camunda/storage/gruppe-4/u-twittermessagermessage/images/2018/09/u-twittermessage-screen2.png)

5. Alle geänderten Dateien speichern.

## Testen

### Projekt aktualisieren
1. Projekt mit _F5-Taste_ refreshen
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven clean_ um Maven zurückzusetzen
3.  Rechtsklick auf Projektordner  _Maven-> Update Project ..._  dann Projekt anhaken (**Checkboxen beachten! siehe unten**) und _OK_

![general-screen1](camunda/storage/gruppe-4/u-twittermessagermessage/images/2018/07/general-screen1.png)

### Test starten

Rechtsklick auf Java-Datei `learnbpm.example.test.twittermessage.UnitTestProzess ` und dann _Run As -> JUnit Test_

**Ergebnisse unter [https://twitter.com/camunda_demo](https://twitter.com/camunda_demo)
 **

![u-twittermessage-screen3](camunda/storage/gruppe-4/u-twittermessagermessage/images/2018/09/u-twittermessage-screen3.png)

**Info:**
Die Testmethoden `testProzess` und `testTeilprozess` wurden vorab erstellt. Eine andere Übung beschäftigt sich mit dem Erstellen von Tests.

## Projekt deployen (in Camunda-Engine spielen)

1. Projekt aktualisieren (siehe [Projekt aktualisieren](#projekt-aktualisieren))
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven build..._
3. Im Konfigurationsfenster unter Goals `tomcat7:deploy` beim ersten deployen eingeben; ist das Projekt schon mal deployed worden, dann `tomcat7:redeploy` eingeben und mit _OK_ ausführen


## Prozess in camunda starten
1. Im Browser die Tasklist öffnen: `http://localhost:8080/camunda/app/tasklist/default/#/login`
2. Mit Admin einloggen (demo/demo)
3. Rechts oben "Start process" anklicken
4. Prozess im Fenster durch Anklicken auswählen
5. Im Folgefenster roten Knopf "Start" betätigen


## Doku/ weiterführende Quellen

## Lösung

Die Lösung finden Sie im gleichnamigen eclipse-Projekt mit der Namensergänzung "-loesung"
