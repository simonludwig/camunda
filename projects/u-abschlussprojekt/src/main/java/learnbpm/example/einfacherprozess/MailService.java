package java.learnbpm.example.einfacherprozess;

import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;



public class MailService {

    private static final String HOST = "mailcatcher";
    private static final String USER = "team-1@camunda.com";
    private static final Integer PORT = 1025;

    public void sendEmail(String to, String subject, String content) throws EmailException {
        HtmlEmail email = new HtmlEmail();
        email.setHostName(HOST);
        email.setSmtpPort(PORT);
        email.setDebug(true);
        email.setSSLOnConnect(false);
        email.setHtmlMsg(content);
        email.setCharset("utf-8");

        // Just in case HTML is unsupported
        email.setTextMsg("Dein E-Mail-Client unterstützt kein HTML.");
        email.addTo(to);
        email.setFrom(USER);
        email.setSubject(subject);
        email.send();
    }
}
/*
        Email email = new SimpleEmail();
        email.setCharset("utf-8");
        email.setHostName(HOST);
        email.setSmtpPort(PORT);
        email.setSSLOnConnect(false);
        email.setContent(content, "text/html; charset=utf-8");
        email.setFrom(USER);
        email.setSubject(subject);
        email.setMsg(content);
        email.addTo(to);

        email.send();
    }
}
*/
