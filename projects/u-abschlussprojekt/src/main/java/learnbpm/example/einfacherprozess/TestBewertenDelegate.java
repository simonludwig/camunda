package java.learnbpm.example.einfacherprozess;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class TestBewertenDelegate implements JavaDelegate {


	@Override
	public void execute(DelegateExecution execution) throws Exception {

        double testErgebnis = (double)execution.getVariable("survey_result");

            if (testErgebnis >= 80){
                execution.setVariable("zugelassen", true);
            }else{
                execution.setVariable("zugelassen", false);
            }
    }

}
