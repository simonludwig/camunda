package java.learnbpm.example.einfacherprozess;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.value.FileValue;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.SQLDialect;
import org.jooq.Table;
import org.jooq.impl.DSL;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;

public class CreateUserDelegate implements JavaDelegate {

    final String VARIABLE_BEWERBER_EMAIL = "apply_email";

    final String HTML_FIELD_LASTNAME = "nachname";
    final String HTML_FIELD_FIRSTNAME = "vorname";
    final String HTML_FIELD_HZB = "hzb_file";
    final String HTML_FIELD_INSURANCE = "insurance";
    final String HTML_DEGREE = "degree";
    final String HTML_NATIONALITY = "nationality";
    final String HTML_BIRTHDAY = "birthdate";
    final String HTML_MAIL = "mail";
    final String HTML_NC = "nc";
    final String HTML_COURSE = "course";

    final Table APPLY_TABLE = DSL.table("bewerber");
    final Field<String> FIRST_NAME_FIELD = DSL.field("first_name", String.class);
    final Field<String> LAST_NAME_FIELD = DSL.field("last_name", String.class);
    final Field<Date> BIRTHDATE_FIELD = DSL.field("birth", Date.class);
    final Field<String> NATIONALITY_FIELD = DSL.field("nationality", String.class);
    final Field<String> INSURANCE_FIELD = DSL.field("insurance", String.class);
    final Field<String> DEGREE_FIELD = DSL.field("degree", String.class);
    final Field<String> CHECKED_BY = DSL.field("checked_by", String.class);
    final Field<Double> NC = DSL.field("nc", Double.class);
    final Field<String> FILE_PATH = DSL.field("file_path", String.class);
    final Field<String> MAIL = DSL.field("mail", String.class);
    final Field<String> COURSE = DSL.field("course", String.class);
    final Field<String> ACCEPTED = DSL.field("accepted", String.class);
    final Field<String> MATRNR = DSL.field("matrikelnr", String.class);


    final Table studiengang_TABLE = DSL.table("studiengang");
    final Field<String> stud_id = DSL.field("stud_id", String.class);
    final Field<String> stud_name = DSL.field("stud_name", String.class);
    final Field<String> stud_zulassungstyp = DSL.field("stud_zulassungstyp", String.class);


    @Inject
    IdentityService identityService;

    public CreateUserDelegate() {
    }

    private void createApplyTable(DSLContext dsl) {
        dsl.createTableIfNotExists(APPLY_TABLE)
                .column(FIRST_NAME_FIELD)
                .column(LAST_NAME_FIELD)
                .column(BIRTHDATE_FIELD)
                .column(NATIONALITY_FIELD)
                .column(INSURANCE_FIELD)
                .column(CHECKED_BY)
                .column(MAIL)
                .column(NC)
                .column(DEGREE_FIELD)
                .column(COURSE)
                .column(FILE_PATH)
                .column(ACCEPTED)
                .column(MATRNR).execute();
    }

    private void createStudiengang(DSLContext dsl) {
        dsl.createTableIfNotExists(studiengang_TABLE)
                .column(stud_id)
                .column(stud_name)
                .column(stud_zulassungstyp).execute();
    }

    enum Type {
            NC("nc"), TEST("test"), UN("unbeschränkt");
        private String typ;

        Type(String typ) {
            this.typ = typ;
        }

        public String getTyp() {
            return typ;
        }

        public void setTyp(String typ) {
            this.typ = typ;
        }
    }

    private void insertFakeData(DSLContext dsl) {

        Integer count = dsl.selectCount().from(studiengang_TABLE).fetchOne(0, int.class);
        if (count != null && count >= 1) {
            return;
        }


        dsl.insertInto(studiengang_TABLE).set(stud_id, "THM-2021-k-1").set(stud_name, "Wirtschaftsinformatik")
                .set(stud_zulassungstyp, Type.UN.getTyp())
                .execute();

        dsl.insertInto(studiengang_TABLE).set(stud_id, "THM-2021-k-2").set(stud_name, "Biologie")
                .set(stud_zulassungstyp, Type.TEST.getTyp())
                .execute();

        dsl.insertInto(studiengang_TABLE).set(stud_id, "THM-2021-k-3").set(stud_name, "Mathematik")
                .set(stud_zulassungstyp, Type.NC.getTyp())
                .execute();

        dsl.insertInto(studiengang_TABLE).set(stud_id, "THM-2021-k-4").set(stud_name, "Betriebswirtschaftslehre")
                .set(stud_zulassungstyp, Type.UN.getTyp())
                .execute();

        dsl.insertInto(studiengang_TABLE).set(stud_id, "THM-2021-k-5").set(stud_name, "Chemie")
                .set(stud_zulassungstyp, Type.NC.getTyp())
                .execute();

        dsl.insertInto(studiengang_TABLE).set(stud_id, "THM-2021-k-6").set(stud_name, "Journalismus")
                .set(stud_zulassungstyp, Type.TEST.getTyp())
                .execute();

        dsl.insertInto(studiengang_TABLE).set(stud_id, "THM-2021-k-7").set(stud_name, "Anglistik")
                .set(stud_zulassungstyp, Type.UN.getTyp())
                .execute();

        dsl.insertInto(studiengang_TABLE).set(stud_id, "THM-2021-k-8").set(stud_name, "Physik")
                .set(stud_zulassungstyp, Type.NC.getTyp())
                .execute();

        dsl.insertInto(studiengang_TABLE).set(stud_id, "THM-2021-k-9").set(stud_name, "Marketing")
                .set(stud_zulassungstyp, Type.TEST.getTyp())
                .execute();
    }

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        System.out.println("Saving FILE");
        Connection connection = DriverManager.getConnection("jdbc:postgresql://postgres:5432/camunda", "camunda", "camunda");
        DSLContext dsl = DSL.using(connection, SQLDialect.POSTGRES);

        createApplyTable(dsl);
        createStudiengang(dsl);
        insertFakeData(dsl);

        int result = dsl.insertInto(APPLY_TABLE)
                .set(FIRST_NAME_FIELD, execution.getVariableTyped(HTML_FIELD_FIRSTNAME).getValue())
                .set(LAST_NAME_FIELD, execution.getVariableTyped(HTML_FIELD_LASTNAME).getValue())
                .set(BIRTHDATE_FIELD, execution.getVariableTyped(HTML_BIRTHDAY).getValue())
                .set(INSURANCE_FIELD, execution.getVariableTyped(HTML_FIELD_INSURANCE).getValue())
                .set(NATIONALITY_FIELD, execution.getVariableTyped(HTML_NATIONALITY).getValue())
                //.set(INSURANCE_FIELD, execution.getVariableTyped(HTML_DEGREE).getValue())
                .set(MAIL, execution.getVariableTyped(HTML_MAIL).getValue())
                .set(DEGREE_FIELD, execution.getVariableTyped(HTML_DEGREE).getValue())
                .set(NC, execution.getVariableTyped(HTML_NC).getValue())
                .set(COURSE, execution.getVariableTyped(HTML_COURSE).getValue())
                .set(FILE_PATH, execution.getVariableTyped(HTML_FIELD_HZB).getValue())
                .set(ACCEPTED, "N/A")
                .set(MATRNR, "N/A").execute();


        execution.setVariable("BEWERBER_ID", result);
        execution.setVariable(VARIABLE_BEWERBER_EMAIL, execution.getVariableTyped(HTML_MAIL).getValue());
    }

}
