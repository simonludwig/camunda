package java.learnbpm.example.einfacherprozess;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;


public class SendEmailDelegate implements JavaDelegate {

	@Override
	public void execute(DelegateExecution execution) throws Exception {
        String content = MailTemplate.mailTemplate;
        System.out.println(execution.getVariable("email"));
        if("MISSING_DOCUMENTS".equals(execution.getCurrentActivityId())){
            content = content.replace("<#nested>", "Guten Tag,\n Leider fehlen uns noch Unterlagen, um Ihren Immatrikulationsprozess weiter durchzuführen.\n Bitte reichen Sie uns Ihre Unterlagen umgehend zu, sodass wir Ihre Immatrikulation erfolgreich abschließen können.\n \nMit freundlichen Grüßen\nIhr THM-Sekretariat");
            new MailService().sendEmail(execution.getVariable("mail").toString(),"THM-Anmeldeverfahren", content);
            //new MailService().sendEmail(execution.getVariable("mail").toString(),"THM-Anmeldeverfahren","Guten Tag,\n Leider fehlen uns noch Unterlagen, um Ihren Immatrikulationsprozess weiter durchzuführen.\n Bitte reichen Sie uns Ihre Unterlagen umgehend zu, sodass wir Ihre Immatrikulation erfolgreich abschließen können.\n \nMit freundlichen Grüßen\nIhr THM-Sekretariat");
        }else{
            content = content.replace("<#nested>", "Guten Tag,\n Ihre Unterlagen haben uns vollständig erreicht und wir werden nun die nächsten Schritte im Immatrikulationsprozess einleiten.\n \nMit freundlichen Grüßen\nIhr THM-Sekretariat");
            new MailService().sendEmail(execution.getVariable("mail").toString(),"THM-Anmeldeverfahren", content);
            //new MailService().sendEmail(execution.getVariable("mail").toString(),"THM-Anmeldeverfahren","Guten Tag,\n Ihre Unterlagen haben uns vollständig erreicht und wir werden nun die nächsten Schritte im Immatrikulationsprozess einleiten.\n \nMit freundlichen Grüßen\nIhr THM-Sekretariat");
        }
	}
}
