package java.learnbpm.example.einfacherprozess;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.value.FileValue;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Result;
import org.jooq.ResultQuery;
import org.jooq.SQLDialect;
import org.jooq.Table;
import org.jooq.impl.DSL;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class GenerateMatrikelDelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        System.out.println("Matrikelnummer generieren und eintragen..");
        MatrikelNummerService mns = new MatrikelNummerService();
        Connection connection = DriverManager.getConnection("jdbc:postgresql://postgres:5432/camunda", "camunda", "camunda");
        Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_UPDATABLE);

        String sql = String.format("Update bewerber set matrikelnr = %s", mns.generateMatrikelnummer().toString());

        statement.executeUpdate(sql);
    }
}
