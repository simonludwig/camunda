package java.learnbpm.example.einfacherprozess;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class ZugangsdatenMailDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        String content = MailTemplate.mailTemplate;
        String processInstanceId = execution.getProcessInstanceId();
        System.out.println(execution.getVariable("mail"));
        content = content.replace("<#nested>", "Guten Tag,\nFür den von Ihnen gewählten Studiengang wird ein Test als Zulassungsbeschränkung vorgesehen. Über den nachfolgenden Link werden Sie zum Test weitergeleitet.\nLink zum Test: http://videobakers.pt:9999/survey/" + processInstanceId + "\n \nViel Erfolg & viele Grüße\nIhr THM-Sekretariat");
        new MailService().sendEmail(execution.getVariable("mail").toString(), "THM-Anmeldeverfahren", content);
        //"Guten Tag,\nFür den von Ihnen gewählten Studiengang wird ein Test als Zulassungsbeschränkung vorgesehen. Über den nachfolgenden Link werden Sie zum Test weitergeleitet.\nLink zum Test: localhost:9999/surveyjs.html\nProcessID = %s \n \nViel Erfolg & viele Grüße\nIhr THM-Sekretariat"
    }

}
