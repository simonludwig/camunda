package java.learnbpm.example.einfacherprozess;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class AbholungMailDelegate implements JavaDelegate {

	@Override
	public void execute(DelegateExecution execution) throws Exception {
        String content = MailTemplate.mailTemplate;
        System.out.println(execution.getVariable("mail"));
        content = content.replace("<#nested>", "Guten Tag,\nda Ihre Zahlung erfolgreich eingegangen ist, haben wir den Druck Ihres Studentenausweises bereits in Auftrag gegeben.\n \nSie können Diesen Anfang nächster Woche im Studiensekretariat abholen.\n \nMit freundlichen Grüßen\nIhr THM-Sekretariat");
        new MailService().sendEmail(execution.getVariable("mail").toString(),"THM-Anmeldeverfahren", content);
        //"Guten Tag,\nda Ihre Zahlung erfolgreich eingegangen ist, haben wir den Druck Ihres Studentenausweises bereits in Auftrag gegeben.\n \nSie können Diesen Anfang nächster Woche im Studiensekretariat abholen.\n \nMit freundlichen Grüßen\nIhr THM-Sekretariat"
	}

}
