package java.learnbpm.example.einfacherprozess;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.SQLDialect;
import org.jooq.Table;
import org.jooq.impl.DSL;

import javax.inject.Inject;

public class AcceptDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        new MailService().sendEmail((String)execution.getVariable("bewerber"),
                        "Sie wurden angenommen",
                "Test");
    }

}
