package java.learnbpm.example.einfacherprozess;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class AblehnungMailDelegate implements JavaDelegate {

	@Override
	public void execute(DelegateExecution execution) throws Exception {
        String content = MailTemplate.mailTemplate;
        content = content.replace("<#nested>","Guten Tag, es tut uns leid Ihnen mitteilen zu müssen, dass sie abgelehnt wurden.. \n \nMit freundlichen Grüßen\nIhr THM-Sekretariat");
        new MailService().sendEmail(execution.getVariable("mail").toString(),"THM-Anmeldeverfahren", content);
        //"Guten Tag, es tut uns leid Ihnen mitteilen zu müssen, dass sie abgelehnt wurden.. \n \nMit freundlichen Grüßen\nIhr THM-Sekretariat"
	}

}
