package java.learnbpm.example.einfacherprozess;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class ZulassungMailDelegate implements JavaDelegate {

	@Override
	public void execute(DelegateExecution execution) throws Exception {
        String content = MailTemplate.mailTemplate;
        System.out.println(execution.getVariable("mail"));
        content = content.replace("<#nested>", "Guten Tag,\nes freut uns Ihnen mitteilen zu können, dass Sie angenommen wurden.\n \nUm Ihre Immatrikulation erfolgreich abzuschließen bitten wir Sie die Studiengebühren in Höhe von 280,32 € in den nächsten zwei Wochen zu überweisen \n Mit freundlichen Grüßen\nIhr THM-Sekretariat");
        new MailService().sendEmail(execution.getVariable("mail").toString(),"THM-Anmeldeverfahren", content);
        //"Guten Tag,\nes freut uns Ihnen mitteilen zu können, dass Sie angenommen wurden.\n \nUm Ihre Immatrikulation erfolgreich abzuschließen bitten wir Sie die Studiengebühren in Höhe von 280,32 € in den nächsten zwei Wochen zu überweisen \n Mit freundlichen Grüßen\nIhr THM-Sekretariat"
	}

}
