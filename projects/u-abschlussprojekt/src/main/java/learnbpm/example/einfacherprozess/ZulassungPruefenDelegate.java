package java.learnbpm.example.einfacherprozess;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.value.FileValue;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Result;
import org.jooq.ResultQuery;
import org.jooq.SQLDialect;
import org.jooq.Table;
import org.jooq.impl.DSL;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

public class ZulassungPruefenDelegate implements JavaDelegate {

    final Table STUD_TABLE = DSL.table("studiengang");
    final Table BEWERBER_TABLE = DSL.table("bewerber");
    final Field<String> STUD_ID_FIELD = DSL.field("stud_id", String.class);
    final Field<String> STUD_NAME_FIELD = DSL.field("stud_name", String.class);
    final Field<String> STUD_TYP_FIELD = DSL.field("stud_zulassungstyp", String.class);
    final Field<String> KURS_ID_FIELD = DSL.field("course", String.class);


     @Override
    public void execute(DelegateExecution execution) throws Exception {
        System.out.println("Prüfe Zulassung..");
        Connection connection = DriverManager.getConnection("jdbc:postgresql://postgres:5432/camunda", "camunda", "camunda");
        Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_UPDATABLE);
        String mail = execution.getVariable("mail").toString();
        String sql = "select stud_zulassungstyp from bewerber, studiengang where course = stud_id AND mail = '"+mail+"';";

        ResultSet res = statement.executeQuery(sql);

		// Cursor des ResultSets auf die erste Position setzen
		res.first();

		// Objekt der Ersten Spalte holen, als String casten und der Variable einTippername zuweisen
        String zulassungstyp = (String) res.getObject(1);

        if (zulassungstyp.equals("unbeschränkt")){
            execution.setVariable("zugelassen", true);
        }else{
            execution.setVariable("zugelassen", false);
        }

        execution.setVariable("zulassungstyp", zulassungstyp);
    }
}
