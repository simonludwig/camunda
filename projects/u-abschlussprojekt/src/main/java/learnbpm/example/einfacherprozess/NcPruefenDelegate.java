package java.learnbpm.example.einfacherprozess;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class NcPruefenDelegate implements JavaDelegate {

	@Override
	public void execute(DelegateExecution execution) throws Exception {

        double ncStudent = (double)execution.getVariable("nc");
        double ncErmittelt = (double)execution.getVariable("nc_ermittelt");
            if (ncStudent <= ncErmittelt){
                execution.setVariable("zugelassen", true);
            }else{
                execution.setVariable("zugelassen", false);
            }
    }

}
