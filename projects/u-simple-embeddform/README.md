# Übung "Einfache Embedded Form"
In dieser Übung werden User-Tasks erzeugt, und die Eingabemasken zu den User-Tasks als Embedded Task Forms als HTML-Dateien erstellt. Diese HTML-Dateien werden mit den User-Tasks verknüpft, so dass die Prozess-Engine bei Ausführung der User-Tasks die entsprechende HTML-Eingabemaske lädt. Die Eingabemasken werden über die Tasklist-Applikation von Camunda genutzt.

## Voraussetzungen

1. Eclipse-Projekt `u-simple-embeddform` importieren
2. ggf. POM anpassen

## Übung

1. Prozessmodell erstellen
  1. Prozessmodelldatei  `/u-simple-embeddform/src/main/resources/u-simple-embeddform.bpmn`  mit Camunda-Modeler öffnen
  2. Prozesselemente wie nachfolgend abgebildet erstellen

![u-simple-embeddform-screen1](camunda/storage/gruppe-4/u-simple-embeddformbeddform/images/2018/09/u-simple-embeddform-screen1.png)

2. Für die User-Tasks in dem Prozessmodell sogenannte [Embedded Task Forms](https://docs.camunda.org/manual/7.8/user-guide/task-forms/) erstellen. Diese Forms sind HTML-Dateien die um Direktiven ergänzt werden um beispielsweise Prozessvariablen in einer Form zu erzeugen. mit Werten zu belegen oder auch nur anzuzeigen. Im User-Task wird im Prozessmodell der "Link" zur Form eingefügt, die bei Ausführung des User-Tasks in der Tasklist-Applikation aufgerufen werden soll.

  Alle HTML-Dateien für Embedded Task Forms müssen im Projektordner `src/main/webapp/forms`angelegt werden (in diesem Projekt: `/u-simple-embeddform/src/main/webapp/forms`).

  1. Erstellen der Datei **tippeingabe.html** für den User-Task "Tipp eingeben"

    Erzeugen Sie eine HTML-Datei mit dem Namen `tippeingabe.html` in dem obigen Ordner. Sie können die Datei (je nach Bedarf) in ecplise mit dem  _HTML-Editor_ (Quelltext angezeigt) oder dem _Web Page Editor_ (Quelltext und Webseite angezeigt) bearbeiten. Wählen Sie dazu die Datei mit der rechten Maustaste aus und gehen Sie in das Kontextmenü _Open With_ und wählen den entsprechenden Editor.

    ![u-simple-embeddform-screen8](images/2018/09/u-simple-embeddform-screen8.png)

    Legen Sie in der Date das Format und den Schriftsatz fest und erstellen Sie eine Überschrift für die Seite.

    ```HTML
    <meta http-equiv="content-type" content="text/html; charset=utf-8">

    <h2>Bitte Tippername und Spieltipp eingeben </h2>
    ```

    Anschließend wird eine Form definiert innerhalb derer Werte für Prozessvariablen eigegeben werden sollen

    ```HTML
    <form role="form"
          name="variablesForm">
    ```

    Da die Tasklist-Applikation auch die javascript-Bibliothek bootstrap unterstützt, wird im folgenden für die Form ein responsives [grid-Layout](http://holdirbootstrap.de/css/#grid-example-mixed-complete) verwendet. Dieses Layout ist aber optional!

    Anlegen eines Grid und einer Spalte für die Eingabe persönlicher Daten
    ```HTML
    <div class="row">
      <div class="col-xs-6">
        <h3>Persönliche Daten</h3>
    ```

    Erstellen eines Eingabefelds und Verknüpfung mit der Prozessvariablen `name` vom Typ `String`. Verknüpfung erfolgt über die Direktiven `cam-variable-name` und `cam-variable-type`. Festlegen, dass die Eingabe für die Form verpflichtend ist (`required`). Der restliche Code dient dem Layouting.
    ```HTML
    <div class="control-group">
      <label class="control-label">Name</label>
      <div class="controls">
      <!--
      Erfassung des Tippernamens und Speichern in der Prozessvariable name
      camunda-ID der  Variable  über  cam-variable-name
      camunda-Type der Varibale über  cam-variable-type; Datentypen immer Anfangsbuchstaben groß
          -->
        <input type="text"
               cam-variable-name="name"
               cam-variable-type="String"
               required
               class="form-control" />
      </div>
    </div>
    </div>
    ```

    Erstellen eines weieren Texteingabfeldes das "Spieltipp" heißen soll und eine Erzeugung und Belegung der Prozessvariablen `tipp` vom Typ `String` ermöglicht. Gehen Sie analog der Erstellung des vorherigen Eingabefelds vor!
    ```HTML
    <div class="col-xs-6">
      <h3>Tippdaten</h3>
      <!--
      ... TODO ...
        -->
    </div>
    ```
      Schließen der Abschnitte und der Form. Unten ist über die Direktive `camscript` noch ein Part aufgeführt, der für individuellen Javascript-Code genutzt werden kann. Dieser Code wird von der Prozess-Engine bei Aufruf der Seite ausgeführt. In diesem Beispiel wird er nicht benötigt/genutzt.

    ```HTML
    </div>
    <script cam-script type="text/form-script">
    // custom JavaScript goes here
    </script>
    </form>
    ```

  2. Erstellen der Datei **show_tippeingabe.html** für den User-Task "Tipp anzeigen"

      In dieser Datei sollen Tippername und Spieltipp lediglich in einer Form angezeigt werden. Ein Ändern der Werte soll nicht möglich sein. Die HTML-Seite unterscheidet sich von `tippeingabe.html` lediglich darin, dass jede `<input ... >`-Direktive um das Attribut `readonly="true"` ergänzt wird, dass das `required`-Attribut nicht gesetzt wird und dass die Angabe des Datentyps der Prozessvariablen über `cam-variable-type=` fehlt. Letzteres ist nicht notwendig, da die Prozessvariable schon existiert. Nur wenn über eine Form eine Prozessvariable erzeugt werden soll, muss auch der Datentyp angegeben werden.

      Nutzen Sie die Datei tippeingabe.html und passen Sie diese entsprechend an.

      Beispielanpassung für das Eingabefeld "Name":
      ```HTML
      <div class="control-group">
          <label class="control-label">Name</label>
          <div class="controls">
            <input type="text"
                   cam-variable-name="name"
                   readonly="true"
                   class="form-control" />
          </div>
        </div>
      ```

3. Verknüpfung der User-Tasks mit den HTML-Dateien im Prozessmodell

  Um eine HTML-Datei mit einem User-Task zu verknüpfen muss der Dateipfad als `embedded:app:forms/` + Dateiname als "Form Key" im User-Task definiert werden.


  ![u-simple-embeddform-screen2](camunda/storage/gruppe-4/u-simple-embeddformbeddform/images/2018/09/u-simple-embeddform-screen2.png)

  ![u-simple-embeddform-screen3](camunda/storage/gruppe-4/u-simple-embeddformbeddform/images/2018/09/u-simple-embeddform-screen3.png)

4. Alle Dateien und Änderungen speichern.

## Testen

Unit-Tests von Prozessmodellen mit User-Tasks werden in dieser Übung nicht behandelt.

## Projekt deployen (in Camunda-Engine spielen)

1. Projekt mit _F5-Taste_ refreshen
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven clean_ um Maven zurückzusetzen
3.  Rechtsklick auf Projektordner  _Maven-> Update Project ..._  dann Projekt anhaken (**Checkboxen unten beachten!**) und _OK_

![general-screen1](camunda/storage/gruppe-4/u-simple-embeddformbeddform/images/2018/08/general-screen1.png)

4. Rechtsklick auf Projektordner und dann  _Run As -> Maven build..._
5. Im Konfigurationsfenster unter Goals `tomcat7:deploy` beim ersten deployen eingeben; ist das Projekt schon mal deployed worden, dann `tomcat7:redeploy` eingeben und mit _OK_ ausführen

## Prozess in camunda starten
1. Im Browser die Tasklist öffnen: `http://localhost:8080/camunda/app/tasklist/default/#/login`
2. Mit Admin einloggen (demo/demo)
3. Rechts oben "Start process" anklicken
4. Prozess im Fenster durch Anklicken auswählen
5. Im Folgefenster roten Knopf "Start" betätigen

## Prozess durchlaufen

1. Task "Tipp eingeben" unter "All Tasks" auswählen und **claimen**!

![u-simple-embeddform-screen4](camunda/storage/gruppe-4/u-simple-embeddformbeddform/images/2018/09/u-simple-embeddform-screen4.png)

2. Tippername und Spieltipp eingeben und completen.

![u-simple-embeddform-screen5](camunda/storage/gruppe-4/u-simple-embeddformbeddform/images/2018/09/u-simple-embeddform-screen5.png)

3. Task "Tipp anzeigen" auswählen und claimen.


![u-simple-embeddform-screen6](camunda/storage/gruppe-4/u-simple-embeddformbeddform/images/2018/09/u-simple-embeddform-screen6.png)


4. "Tipp anzeigen" completen (damit wird auch der Prozess abgeschlossen da hinter dem Task direkt das Endevent folgt).


## Doku/ weiterführende Quellen

[camunda-doku zu User Forms](https://docs.camunda.org/manual/7.9/user-guide/task-forms/)

## Lösung

Die Lösung finden Sie im gleichnamigen eclipse-Projekt mit der Namensergänzung "-loesung"
