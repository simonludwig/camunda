package learnbpm.example.advanced;

import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;

public class TaskNameAendernDelegate implements TaskListener {

	public TaskNameAendernDelegate() {

	}

	@Override
	public void notify(DelegateTask delegateTask) {

		// Variable mit dem Namen "tippername" holen, die im Scope des deligierten Tasks
		// liegen muss; d.h. diese Variable muss der Prozessinstanz zugeordnet sein,
		// in der sich auch der Delegate Task befindet
		String my_tippername = (String) delegateTask.getVariable("tippername");
		
		//zu Debuggingzwecken
		System.out.println("Der alte Name des Tasks lautet: "+ delegateTask.getName());
		
		// dynamisch den Wert des Tippernamens in den Tasknamen einfügen
		delegateTask.setName("Tipp von "+my_tippername);

		//zu Debuggingzwecken
		System.out.println("Der neue Name des Tasks lautet: "+ delegateTask.getName());


	}

}
