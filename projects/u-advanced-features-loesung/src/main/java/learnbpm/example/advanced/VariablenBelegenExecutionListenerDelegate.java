package learnbpm.example.advanced;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.Variables.SerializationDataFormats;

public class VariablenBelegenExecutionListenerDelegate implements ExecutionListener {

	public VariablenBelegenExecutionListenerDelegate() {
		
	}

	@Override
	public void notify(DelegateExecution execution) throws Exception {
	
		// Erzeugen der Prozessvariablen und belgen mit Wert		
		execution.setVariable("tippername","Paul");
				
		
		
		
		
	}

}
