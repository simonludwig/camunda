package learnbpm.example.advanced;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class ProzessLoeschenDelegate implements JavaDelegate {

	public ProzessLoeschenDelegate() {

	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		
		
		// Runtime-Service der laufenden Camunda-Instanz holen
		RuntimeService rtm = execution.getProcessEngineServices().getRuntimeService();

		// Id der Prozessinstanz holen die diese Delegate-Klasse über einen Service-Task
		// aufgerufen hatt
		String pii = execution.getProcessInstanceId();
		
		//optional: Anzeige der Prozessinstanz-ID zu Debugginzwecken
		System.out.println("ProzessinstanzID: " + pii);
		
		// Mit dem Runtimeservice Prozessinstanz über deren Id identifizieren
		// und löschen; verpflichtend auch einen Löschungrund angeben
		rtm.deleteProcessInstance(pii, "Löschen der Prozessinstanz zu Testzwecken"+pii);
		
		//optional: zu Debugginzwecken
		System.out.println("Prozessinstanz "+pii+ " gelöscht");
		

	}

}
