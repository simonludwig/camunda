package learnbpm.example.test.advanced;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.test.ProcessEngineRule;

import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;

import org.camunda.bpm.engine.test.Deployment;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineAssertions.processEngine;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Test case starting an in-memory database-backed Process Engine.
 */
public class UnitTestProzess {

	// Variante 1 (Standard): Testen eines einzigen Prozessmodells

	/*
	 * @ClassRule
	 * 
	 * @Rule public static ProcessEngineRule rule =
	 * TestCoverageProcessEngineRuleBuilder.create().build();
	 * 
	 */

	// Variante 2 wenn man mehr als ein Prozessmodell in einer Testklasse testen
	// will

	// @Rule
	// public ProcessEngineRule rule =
	// TestCoverageProcessEngineRuleBuilder.create().build();

	// hier Variante 1
	@ClassRule

	@Rule
	public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

	static {
		LogFactory.useSlf4jLogging(); // MyBatis
	}

	@Before
	public void setup() {
		init(rule.getProcessEngine());
	}

	// JUnit-Test für den gesamten Prozess
	@Test
	@Deployment(resources = "u-advanced-features.bpmn") // hier die Prozessmodelle aufführen die getest werden sollen
	public void testProzess() {

		// Prozessinstanz des Prozessmodells mit dem Key (=Id des Prozessmodells)
		// erzeugen und starten

		ProcessInstance pi = processEngine().getRuntimeService().startProcessInstanceByKey("u-advanced-features");
		
		//optional: Id der gestarteten Prozessinstanz auf Konsole ausgeben
		System.out.println("Prozessinstanz mit der Id "+ pi.getId()+ " gestartet");
		
				
		
		//---------------------------------------------------------------
		//User-Task "Tipp von ..." simulieren
		//---------------------------------------------------------------
		
	
		// Nach dem User-Task mit DefinitionKey "tipptaskId" suchen
		// Achtung Verwechslungsgefahr: die Id des Tasks im Camunda-Modeler ist der DefinitionKey! 
		// Die so genannte taskId wird von der Engine erzeugt sobald ein Task erzeugt wird (von einer Taskdefinition im Prozessmodell können
		//ja mehrere Tasks erzeugt werden)

		// Task-Service holen
		TaskService taskService = processEngine().getTaskService();

		// über verketteten Methodenaufruf (FLUENT-API) eine Query mit dem Task-Service
		// starten
		// die in diesem Fall nach dem Task mit dem DefinitionKey "tipptaskId" 
		// für die oben gestartete PRozessinstanz sucht und
		// diesen in einer typisierten Java-List zurückgibt
	
		String processistanceId = pi.getId();
	    List<Task> Taskliste = taskService.createTaskQuery().processInstanceId(processistanceId).taskDefinitionKey("tipptaskId").list();
			
	

		// Erstes Element aus Taskliste holen;
		Task meinTask = Taskliste.get(0);
		
		// optional zum Debuggen: Definiton-Key des Tasks auf der Konsole ausgeben
		System.out.println("Definition-Key des User-Tasks: "+meinTask.getTaskDefinitionKey());

		// optional zum Debuggen: Taskname auf der Konsole ausgeben
		System.out.println("Ich bin der Task mit dem Namen: " + meinTask.getName());

		Map<String, Object> variables = new HashMap<String, Object>();
		
		variables.put("spieltipp", "5:0");

		// Task "completen" und Prozessvariablen aus Map übergeben
		taskService.complete(meinTask.getId(), variables);
		
		
		
	}



}
