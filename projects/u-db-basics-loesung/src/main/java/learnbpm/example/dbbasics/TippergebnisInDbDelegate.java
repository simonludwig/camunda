package learnbpm.example.dbbasics;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class TippergebnisInDbDelegate implements JavaDelegate {

	public TippergebnisInDbDelegate() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		// TODO Auto-generated method stub
		
		
		//Prozessvariablen aus Prozess holen
		String spieltipp = (String) execution.getVariable("spieltipp");
		String teams = (String) execution.getVariable("teams");

		// Connection zu  DB aufbauen; DB-Name: tippspiel; user: root; password:
		// root
		Connection connection;

		//Datenbankname: tippspiel
		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/tippspiel?user=root&password=root");

		Statement stmt = connection.createStatement();

		// Insert-Befehl als String erstellen
		// ACHTUNG: hier wird der Tipp dem Tipper mit der Id 1 aus der Tipper-Tabelle
		// zugeordnet
		// dies setzt voraus, dass ein Tipper mit der Id 1 bereits in der DB existiert,
		// sonst gibt es eine Fehlermeldung
		String sql = "INSERT INTO tippspiel.spieltipp (teams, tipp, tipperID) VALUES ('" + teams + "', '"
				+ spieltipp + "', '1')";

		// Optional: Ausgabe des Klassennamens und des SQL-Strings auf der Konsole zu Debuggingzwecken
		System.out.println(this.getClass().getName()+": \n"+sql);
		
		
		// Ausführen des Updates auf der DB
		stmt.executeUpdate(sql);
		
		// Datenbank schließen
		connection.close();


	}

}
