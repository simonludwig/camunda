package learnbpm.example.dbbasics;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class TippernameFromDbDelegate implements JavaDelegate {

	public TippernameFromDbDelegate() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		// TODO Auto-generated method stub


		// Connection zu DB aufbauen; DB-Name: tippspiel; user: root; password:
		// root
		Connection connection;

		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/tippspiel?user=root&password=root");

		Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, 
                        ResultSet.CONCUR_UPDATABLE);

		// 
		// ACHTUNG: hier wird der Tippname des Tippers mit der Id 1 aus der Tipper-Tabelle geholt
		// dies setzt voraus dass er schon existiert
		String sql = "SELECT name FROM tippspiel.tipper WHERE idtipper=1";

		// Optional: Ausgabe des Klassennamens und des SQL-Strings auf der Konsole zu Debuggingzwecken
		System.out.println(this.getClass().getName()+": \n"+sql);
		
		
		// Ausführen des Selects auf der DB
		ResultSet res = stmt.executeQuery(sql);
		
		// Cursor des ResultSets auf die erste Position setzen
		res.first();
		
		// Objekt der Ersten Spalte holen, als String casten und der Variable einTippername zuweisen
		String einTippername = (String) res.getObject(1);
		
		// Variable tippername mit Wert in den Prozess geben
		execution.setVariable("tippername", einTippername);



	}

}
