CREATE DATABASE  IF NOT EXISTS `tippspiel` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tippspiel`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: tippspiel
-- ------------------------------------------------------
-- Server version	5.5.5-10.2.14-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `spieltipp`
--

DROP TABLE IF EXISTS `spieltipp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spieltipp` (
  `idspieltipp` int(11) NOT NULL AUTO_INCREMENT,
  `teams` varchar(45) DEFAULT NULL,
  `tipp` varchar(45) DEFAULT NULL,
  `tipperID` int(11) DEFAULT NULL,
  PRIMARY KEY (`idspieltipp`),
  KEY `tipperID_idx` (`tipperID`),
  CONSTRAINT `tipperID` FOREIGN KEY (`tipperID`) REFERENCES `tipper` (`idtipper`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spieltipp`
--

LOCK TABLES `spieltipp` WRITE;
/*!40000 ALTER TABLE `spieltipp` DISABLE KEYS */;
INSERT INTO `spieltipp` VALUES (2,'Schweiz:Island','1:1',1);
/*!40000 ALTER TABLE `spieltipp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipper`
--

DROP TABLE IF EXISTS `tipper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipper` (
  `idtipper` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipper`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipper`
--

LOCK TABLES `tipper` WRITE;
/*!40000 ALTER TABLE `tipper` DISABLE KEYS */;
INSERT INTO `tipper` VALUES (1,'Emil Erfolglos'),(2,'Gundula Gluecklich'),(3,'Willy Wissend');
/*!40000 ALTER TABLE `tipper` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-30 11:36:33
