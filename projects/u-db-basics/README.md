# Übung "Lesen und schreiben Datenbank und Prozess"
Erstellen eines Projekts in dem ein Tippergebnis erzeugt wird und dieses in eine Datenbank geschrieben wird. Dann den Namen des Tippers aus der DB  holen dem dieser Spieltipp zugeordnet wurde und schließlich alle Daten auf der Konsole ausgeben.

## Voraussetzungen


1. Eclipse-Projekt `u-db-basics` importieren
2. ggf. POM anpassen
3. mySQl (oder MariaDB) installiert und konfiguriert
4. Datenbank über das Skript `..\sqldump\u-db-basics.sql` erzeugen
5. Zugangsdaten für DB festlegen: user=root und password=root

**HINWEIS: AUS VEREINFACHUNGSZWECKEN GENÜGT DAS DB-SCHEMA NICHT DEN NORMALENFORMEN**

## Übung

1. Prozessmodell erstellen
  1. Prozessmodelldatei `/u-db-basics/src/main/resources/u-db-basics.bpmn`  mit Camunda-Modeler öffnen
  2. Prozesselemente erstellen

![u-db-basics-screen1](camunda/storage/gruppe-4/u-db-basicsb-basics/images/2018/08/u-db-basics-screen1.png)

2. Skript-Task "Tippergebnis erzeugen" erstellen, Id `TaskTippergebnisID` festlegen und um Skriptcode ergänzen der die Variablen `teams` und `spieltipp` erzeugt und mit Werten belegt.

![u-db-basics-screen2](camunda/storage/gruppe-4/u-db-basicsb-basics/images/2018/08/u-db-basics-screen2.png)

3. Java-Delegate-Klasse für Service-Task "Tippergebnis in DB" erstellen über die die Werte `spieltipp` und `teams` in die Datenbanktabelle `spieltipp` der Datenbank "tippspiel` geschrieben werden.

  1. Klasse `TippergebnisInDbDelegate.java` im Package `learnbpm.example.dbbasics` anlegen und das Interface `JavaDelegate` implementieren (siehe auch Übung _u-servicetask-delegation_)

```java

public class TippergebnisInDbDelegate implements JavaDelegate {

	public TippergebnisInDbDelegate() {

	@Override
  public void execute(DelegateExecution execution) throws Exception {

	}
```

  2. In der execute-Methode über das execution-Objekt die Methode `getVariable` nutzen um auf die Prozessvariablen der Prozessinstanz über deren Namen zuzugreifen (Cast notwendig)

```java
String spieltipp = (String) execution.getVariable("spieltipp");
String teams = (String) execution.getVariable("teams");
```

  3. Datenbankverbindung aufbauen, Update-String erzeugen und Werte von `spieltipp` und `teams` in Datenbank schreiben. Dabei Zuordnung des Tippers mit der Id `1` zu dem Spieltipp (Tipper mit Id `1` schon in Datenbank vorhanden).

```java
// Connection zu DB aufbauen; DB-Name: tippspiel; user: root; password:
// root

Connection connection;
		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/tippspiel?user=root&password=root");

//Statement erzeugen

ToDo...

// Insert-Befehl als String erstellen
// ACHTUNG: hier wird der Tipp dem Tipper mit der Id 1 aus der Tipper-Tabelle
// zugeordnet
// dies setzt voraus, dass ein Tipper mit der Id 1 bereits in der DB existiert,
// sonst gibt es eine Fehlermeldung

ToDo...

// Ausführen des Updates auf der DB

ToDo...

// Datenbank schließen

ToDo...
```

4. Java-Delegate-Klasse `TippernameFromDbDelegate.java` im Package `learnbpm.example.dbbasics` anlegen und mit dem Service-Task "Tippername aus DB" verknüpfen. In der Klasse den Namen des Tippers mit der Id `1` aus der Datenbank holen und den Wert über eine neue Variable `tippername` an den Prozess zurückgeben (**bitte analog zum vorherigen Service-Task vorgehen**).

```java
public class TippernameFromDbDelegate implements JavaDelegate {

	public TippernameFromDbDelegate() {

	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {

    // Connection zu DB aufbauen; DB-Name: tippspiel; user: root; password:
    // root

    ToDo...

    //Statement erzeugen

    ToDo...

		// SQL-String erzeugen der den "name" des "tipper" mit der "idtipper" 1 aus der Tabelle "tippspiel" holt
		// ACHTUNG: hier wird der Tippname des Tippers mit der Id 1 aus der Tipper-Tabelle geholt
		// dies setzt voraus dass er schon existiert

    ToDo...

		// Ausführen des Selects auf der DB mit executeQuery und erhalten eines ResulSet

    ToDo...

		// Cursor des ResultSets auf die erste Position setzen

    ToDo...

		// Objekt der Ersten Spalte holen, als String casten und der Variable einTippername zuweisen

		// Variable tippername erzeugen mit dem Wert aus der DB belegen und an den Prozess übergeben

    execution.setVariable("tippername", einTippername);

  }

  // Datenbank schließen

  ToDo...
```

5. Prozessvariablen `teams` `spieltipp` und den aus der DB geholten `tippername` über einen Skript-Task auf der Konsole ausgeben.

![u-db-basics-screen3](camunda/storage/gruppe-4/u-db-basicsb-basics/images/2018/08/u-db-basics-screen3.png)


6. Alle geänderten Dateien speichern

## Testen


### Projekt aktualisieren
1. Projekt mit _F5-Taste_ refreshen
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven clean_ um Maven zurückzusetzen
3.  Rechtsklick auf Projektordner  _Maven-> Update Project ..._  dann Projekt anhaken (**Checkboxen beachten! siehe unten**) und _OK_

![general-screen1](camunda/storage/gruppe-4/u-db-basicsb-basics/images/2018/07/general-screen1.png)

### Test starten

Rechtsklick auf Java-Datei `/u-processvar-delegate/src/test/java/learnbpm/example/test/dbbasics/UnitTestProzess.java`  und dann _Run As -> JUnit Test_

**Ergebnisse Konsole:**

![u-db-basics-screen4](camunda/storage/gruppe-4/u-db-basicsb-basics/images/2018/08/u-db-basics-screen4.png)

**Inhalte der Tabelle "spieltipp" in der Datenbank "tippspiel" (Fenster des SQL-Tools MySQL Workbench):**

![u-db-basics-screen5](camunda/storage/gruppe-4/u-db-basicsb-basics/images/2018/08/u-db-basics-screen5.png)

**Info:**
Die Testmethoden `testProzess` und `testTeilprozess` wurden vorab erstellt. Eine andere Übung beschäftigt sich mit dem Erstellen von Tests.

## Projekt deployen (in Camunda-Engine spielen)

1. Projekt aktualisieren (siehe [Projekt aktualisieren](#projekt-aktualisieren))
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven build..._
3. Im Konfigurationsfenster unter Goals `tomcat7:deploy` beim ersten deployen eingeben; ist das Projekt schon mal deployed worden, dann `tomcat7:redeploy` eingeben und mit _OK_ ausführen


## Prozess in camunda starten
1. Im Browser die Tasklist öffnen: `http://localhost:8080/camunda/app/tasklist/default/#/login`
2. Mit Admin einloggen (demo/demo)
3. Rechts oben "Start process" anklicken
4. Prozess im Fenster durch Anklicken auswählen
5. Im Folgefenster roten Knopf "Start" betätigen


## Doku/ weiterführende Quellen

## Lösung

Die Lösung finden Sie im gleichnamigen eclipse-Projekt mit der Namensergänzung "-loesung"
