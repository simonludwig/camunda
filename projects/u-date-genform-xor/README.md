# Übung "Generated Form für Datum und XOR-Gateway"
Erstellen eines Prozesses mit zwei User Tasks (als Generated Task Forms) über die das Tippfristende und das Datum des aktellen Tipps eingegeben werden. Danach eine XOR-Verzweigung mit Vergleich der beiden Daten und Ausgaben über Script-Tasks, ob der Tipp verspätet oder fristgerecht abgegegeben wurde.

## Voraussetzungen


1. Eclipse-Projekt `u-date-genform-xor` importieren
2. ggf. POM anpassen

## Übung

1. Prozessmodell erstellen
  1. Prozessmodelldatei  `/u-xor-bool/src/main/resources/u-date-genform-xor.bpmn` mit Camunda-Modeler öffnen
  2. Prozesselemente wie nachfolgend abgebildet erstellen

![u-date-genform-xor-screen1](camunda/storage/gruppe-4/u-date-genform-xorform-xor/images/2018/08/u-date-genform-xor-screen1.png)

2. Im User-Task "Tippfristende eingeben" eine sogenannte [_Generated Task Form_](https://docs.camunda.org/manual/7.9/user-guide/task-forms/#generated-task-forms) erstellen über die der Nutzer ein Datum eingeben kann. Camunda erstellt dann zur Laufzeit eine standardisierte Eingabemaske ohne dass zur Gestaltung HTML-Code verwendet werden muss. Die Gestaltungsmöglichkeiten sind ggü.[ _Embedded Task Forms_](https://docs.camunda.org/manual/7.9/user-guide/task-forms/#embedded-task-forms) sehr beschränkt.
  1. 	Im Reiter _Forms_ der Properties des User-Task  über das Plussymbol ein neues Dialogfeld erzeugen

  ![u-date-genform-xor-screen2](camunda/storage/gruppe-4/u-date-genform-xorform-xor/images/2018/08/u-date-genform-xor-screen2.png)

  2. Verpflichtend eine _ID_ und den _Type_ (Datentyp) festlegen. Das _Label_ ist der Anzeigename für das Feld im Dialog und ist optional. In diesem Fall ist ein Feld zur Eingabe des Tippfristendes als Datum anzulegen.

![u-date-genform-xor-screen3](camunda/storage/gruppe-4/u-date-genform-xorform-xor/images/2018/08/u-date-genform-xor-screen3.png)

3. Für den User-Task "Tippdatum eingeben" in dem das Tippdatum eingegeben werden soll analog zum vorangegangenen User Task  verfahren.

![u-date-genform-xor-screen4](camunda/storage/gruppe-4/u-date-genform-xorform-xor/images/2018/08/u-date-genform-xor-screen4.png)

4. Für die XOR-Verzweigung Bedingungen anlegen so dass nach einem Datumsvergleich entweder der eine oder oder andere Ast durchlaufen wird.

  1. Für den Ast "Tipp fristgerecht" die Bedingungen als "Expression" erstellen. Der Ausdruck `${tippdatum < tippfristende}` vergleicht die Werte der zwei Datumsprozessvariablen. Wenn das Tippdatum kleiner als das Tippfristende ist, ist die Aussage wahr und der Ast wird durchlaufen.

![u-date-genform-xor-screen5](camunda/storage/gruppe-4/u-date-genform-xorform-xor/images/2018/08/u-date-genform-xor-screen5.png)

  2. Für den Ast "Tipp verspaetet" analog verfahren.

![u-date-genform-xor-screen6](camunda/storage/gruppe-4/u-date-genform-xorform-xor/images/2018/08/u-date-genform-xor-screen6.png)

5. In den Skript-Tasks "Ausgabe" jeweils die Javascript-Methode
  ```javascript
print("eine Nachricht")
  ```
mit der passenden Nachricht je nach Verzweigungsast einfügen.

![u-date-genform-xor-screen7](camunda/storage/gruppe-4/u-date-genform-xorform-xor/images/2018/08/u-date-genform-xor-screen7.png)

![u-date-genform-xor-screen8](camunda/storage/gruppe-4/u-date-genform-xorform-xor/images/2018/08/u-date-genform-xor-screen8.png)

6. Datei speichern

## Testen

Unit-Test von Prozessmodellen mit User-Tasks werden in dieser Übung nicht behandelt.

## Projekt deployen (in Camunda-Engine spielen)

1. Projekt mit _F5-Taste_ refreshen
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven clean_ um Maven zurückzusetzen
3.  Rechtsklick auf Projektordner  _Maven-> Update Project ..._  dann Projekt anhaken (**Checkboxen unten beachten!**) und _OK_

![general-screen1](camunda/storage/gruppe-4/u-date-genform-xorform-xor/images/2018/08/general-screen1.png)

4. Rechtsklick auf Projektordner und dann  _Run As -> Maven build..._
5. Im Konfigurationsfenster unter Goals `tomcat7:deploy` beim ersten deployen eingeben; ist das Projekt schon mal deployed worden, dann `tomcat7:redeploy` eingeben und mit _OK_ ausführen

## Prozess in camunda starten
1. Im Browser die Tasklist öffnen: `http://localhost:8080/camunda/app/tasklist/default/#/login`
2. Mit Admin einloggen (demo/demo)
3. Rechts oben "Start process" anklicken
4. Prozess im Fenster durch Anklicken auswählen
5. Im Folgefenster roten Knopf "Start" betätigen

## Prozess durchlaufen

1. Task "Tippfristende eingeben" unter "All Tasks" auswählen und **claimen**!

![u-date-genform-xor-screen9](camunda/storage/gruppe-4/u-date-genform-xorform-xor/images/2018/08/u-date-genform-xor-screen9.png)

2. Datum über Date-Picker auswählen und mit _Complete_ den Task abschließen.

![u-date-genform-xor-screen10](camunda/storage/gruppe-4/u-date-genform-xorform-xor/images/2018/08/u-date-genform-xor-screen10.png)

3. Analog den Task "Tippdatum eingeben" durchführen

![u-date-genform-xor-screen11](camunda/storage/gruppe-4/u-date-genform-xorform-xor/images/2018/08/u-date-genform-xor-screen11.png)

**Ergebnis (im Konsolenfenster des Tomcat): **

![u-date-genform-xor-screen12](camunda/storage/gruppe-4/u-date-genform-xorform-xor/images/2018/08/u-date-genform-xor-screen12.png)


## Doku/ weiterführende Quellen

[camunda-doku zu User Forms](https://docs.camunda.org/manual/7.9/user-guide/task-forms/)

## Lösung

Die Lösung finden Sie im gleichnamigen eclipse-Projekt mit der Namensergänzung "-loesung"
