# Übung "Lesen und schreiben Datenbank Wahrheitswerte (Bool) und Datumsformate"
Erstellen eines Prozesses in dem über eine Embedded-Task-Form mit Date-Picker ein Tippdatum vom Nutzer eingeben wird. Dieses Datum wird dann über einen Service-Task ein eine Datenbank geschrieben. Anschließend wird über einen weiteren Service-Task ein anderes Datum aus der Datenbank gelesen und durch einen Skript-Task auf der Konsole ausgegeben.

## Voraussetzungen


1. Eclipse-Projekt `u-db-bool-date` importieren
2. ggf. POM anpassen
3. mySQl (oder MariaDB) installiert und konfiguriert
3. Datenbank über das Skript `..\sqldump\u-db-bool-date.sql` erzeugen
4. Zugangsdaten für DB festlegen: user=root und password=root

**HINWEIS: AUS VEREINFACHUNGSZWECKEN GENÜGT DAS DB-SCHEMA NICHT DEN NORMALENFORMEN**

## Übung

1. Prozessmodell erstellen
  1. Prozessmodelldatei `/u-db-bool-date/src/main/resources/u-db-bool-date.bpmn`  mit Camunda-Modeler öffnen
  2. Prozesselemente erstellen

![u-db-bool-date-screen1](camunda/storage/gruppe-4/u-db-bool-dateool-date/images/2018/08/u-db-bool-date-screen1.png)

2. Im User-Task "Tippdatum eingeben" eine sogenannte [_Generated Task Form_](https://docs.camunda.org/manual/7.9/user-guide/task-forms/#generated-task-forms) erstellen über die der Nutzer ein Datum eingeben kann. Camunda erstellt dann zur Laufzeit eine standardisierte Eingabemaske ohne dass zur Gestaltung HTML-Code verwendet werden muss. Die Gestaltungsmöglichkeiten sind ggü.[ _Embedded Task Forms_](https://docs.camunda.org/manual/7.9/user-guide/task-forms/#embedded-task-forms) sehr beschränkt.
  1. 	Im Reiter _Forms_ der Properties des User-Task  über das Plussymbol ein neues Dialogfeld erzeugen

  2. Verpflichtend eine _ID_ und den _Type_ (Datentyp) festlegen. Das _Label_ ist der Anzeigename für das Feld im Dialog und ist optional. In diesem Fall ist ein Feld zur Eingabe des Tippdatums als Datum anzulegen.

![u-db-bool-date-screen2](camunda/storage/gruppe-4/u-db-bool-dateool-date/images/2018/08/u-db-bool-date-screen2.png)

  3. Als zweites Formfeld den Wahrheitswert `fristgerecht` anlegen der in der Form als Checkbox dargestellt werden wird.

![u-db-bool-date-screen3](camunda/storage/gruppe-4/u-db-bool-dateool-date/images/2018/08/u-db-bool-date-screen3.png)


3. Java-Delegate-Klasse für Service-Task "Tippdatum in DB" erstellen über die die Werte `tippdatum` und `fristgerecht` in die Datenbanktabelle `spieltipp` der Datenbank "tippspiel" schreiben. Der bereits in der Datenbank vorhandene Spieltipp mit der idspieltipp `2` soll über ein UPDATE um Tippdatum und fristgerecht-Wert ergänzt werden.

  1. Klasse `TippdatumToDbDelegate.java` im Package `learnbpm.example.dbbooldate` anlegen und das Interface `JavaDelegate` implementieren (siehe auch Übung _u-servicetask-delegation_)

```java

public class TippdatumToDbDelegate implements JavaDelegate {

	public TippdatumToDbDelegate() {

	@Override
  public void execute(DelegateExecution execution) throws Exception {

	}
```

  2. In der execute-Methode den Quelltext zum Auslesen der Prozessvariablen und schreiben in die Datenbank ergänzen.

  **Da mySQL den Datentyp BOOLEAN nicht kennt, muss mit Umwandlung in einen int (Datentyp TINYINT in mysql) gearbeitet werden. Außerdem muss das Datumsformat der Prozessvariable tippdatum in einen String umgewandelt und in das SQL-Format (yyyy-MM-dd) umgewandelt werden.**

```java
//Prozessvariablen aus Prozess holen
Boolean bool_fristgerecht = (Boolean) execution.getVariable("fristgerecht");
Date unformatiert_tippdatum = (Date) execution.getVariable("tippdatum");

//Boolean zu int konvertieren da MySQL boolean nicht verarbeiten kann
    int fristgerecht = Boolean.valueOf(bool_fristgerecht).compareTo(Boolean.FALSE);

    //Datumsformat festlegen
				java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	   //Prozessvariable in String im Format "yyy-MM-dd" umwandeln um in DB zu schreiben
				String tippdatum = sdf.format(unformatiert_tippdatum);

// Connection zu DB aufbauen; DB-Name: tippspiel; user: root; password: root

Connection connection;
		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/tippspiel?user=root&password=root");

//Statement erzeugen

ToDo...

// Insert-Befehl als String erstellen
// ACHTUNG: hier werden tippdatum und fristgerecht-Status über ein Update dem Tipp
// mit der id 2 zugeordnet; das setzt voraus, dass dieser Eintrag schon in der DB vorhanden ist
// sonst gibt es eine Fehlermeldung

ToDo...

// Ausführen des Updates auf der DB

ToDo...

// Datenbank schließen

ToDo...
```

4. Java-Delegate-Klasse `TippdatumFromDbDelegate` im Package `learnbpm.example.dbbooldate` anlegen und mit dem Service-Task "Datum des Tipps mit ID 3 aus DB" verknüpfen. In der Klasse das Tippdatum des Spieltipps mit der Id `3` aus der Datenbank holen und den Wert über eine neue Variable`tippdatum2` an den Prozess zurückgeben (**bitte analog zum vorherigen Service-Task vorgehen**)

```java
// Connection zu DB aufbauen; DB-Name: tippspiel; user: root; password: root

ToDo..

//Statement erzeugen

ToDo..


// ACHTUNG: hier wird das tippdatum für den Spieltipp mit der idspieltipp=3 aus der
//Datenbank geholt. Das setzt voraus, dass es den Eintrag gibt sonst gibt es eine
// Fehlermeldung

ToDo..

// Ausführen des Selects auf der DB

ToDo..

// Cursor des ResultSets auf die erste Position setzen

ToDo..

// Wert aus erste Spalte über Methode getDate als Datum auslesen

ToDo..


// Datumsformatierung festlegen

SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

// Formatierung auf die Datumsvariable anwenden und diese als String speichern

String variableMitNeuemFormatAlsString = sdf.format(variableAusDatenbank);


			// Variable mit dem Namen tippdatum2  über Methode setVariable als String in den Prozess geben; unbedingt hier den Namen tippdatum2 vewenden

ToDo..
```

5. Über den Skript-Task "Datum des Tipps ausgeben" das aus der DB geholte Tippdatum auf der Konsole ausgeben

![u-db-bool-date-screen4](camunda/storage/gruppe-4/u-db-bool-dateool-date/images/2018/08/u-db-bool-date-screen4.png)

6. Alle geänderten Dateien speichern.

## Testen

Unit-Test von Prozessmodellen mit User-Tasks werden in dieser Übung nicht behandelt.

## Projekt deployen (in Camunda-Engine spielen)

1. Projekt mit _F5-Taste_ refreshen
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven clean_ um Maven zurückzusetzen
3.  Rechtsklick auf Projektordner  _Maven-> Update Project ..._  dann Projekt anhaken (**Checkboxen unten beachten!**) und _OK_

![general-screen1](camunda/storage/gruppe-4/u-db-bool-dateool-date/images/2018/07/general-screen1.png)

4. Rechtsklick auf Projektordner und dann  _Run As -> Maven build..._
5. Im Konfigurationsfenster unter Goals `tomcat7:deploy` beim ersten deployen eingeben; ist das Projekt schon mal deployed worden, dann `tomcat7:redeploy` eingeben und mit _OK_ ausführen

## Prozess in camunda starten
1. Im Browser die Tasklist öffnen: `http://localhost:8080/camunda/app/tasklist/default/#/login`
2. Mit Admin einloggen (demo/demo)
3. Rechts oben "Start process" anklicken
4. Prozess im Fenster durch Anklicken auswählen
5. Im Folgefenster roten Knopf "Start" betätigen

## Prozess durchlaufen

1. Task "Tippdatum eingeben" unter "All Tasks" auswählen und **claimen**!

![u-db-bool-date-screen5](camunda/storage/gruppe-4/u-db-bool-dateool-date/images/2018/08/u-db-bool-date-screen5.png)

2. Datum über Date-Picker auswählen, Haken bei "fristgerecht eingereicht?" setzen und mit _Complete_ den Task abschließen.

![u-db-bool-date-screen6](camunda/storage/gruppe-4/u-db-bool-dateool-date/images/2018/08/u-db-bool-date-screen6.png)

**Ergebnisse (im Konsolenfenster des Tomcat und der Datenbank): **

**Achtung: **das Datum was auf der Konsole ausgegeben wird ist das aus der Datenbank vom Spieltipp mit der Id 3, nicht das was über die User Form eingegeben wurde. Dies wurde im Spieltipp mit der Id 2 in der Datenbank gespeichert.

![u-db-bool-date-screen7](camunda/storage/gruppe-4/u-db-bool-dateool-date/images/2018/08/u-db-bool-date-screen7.png)

![u-db-bool-date-screen8](camunda/storage/gruppe-4/u-db-bool-dateool-date/images/2018/08/u-db-bool-date-screen8.png)

## Doku/ weiterführende Quellen


## Lösung

Die Lösung finden Sie im gleichnamigen eclipse-Projekt mit der Namensergänzung "-loesung"
