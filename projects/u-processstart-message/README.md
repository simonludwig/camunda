# Übung "Prozess durch Message anstossen"
Prozess erstellen über den eine Prozessvariable festgelegt wird und danach mittels eines Send-Tasks eine Message erzeugt wird die einen zweiten Prozess anstösst und in der die Prozessvariable übergeben wird. Im zweiten Prozess diese Message empfangen und über einen Skript-Task die Prozessvariable auf der Konsole ausgeben.



![u-processstart-message-screen1](camunda/storage/gruppe-4/u-processstart-message-message/images/2018/09/u-processstart-message-screen1.png)

## Voraussetzungen


1. Eclipse-Projekt `u-processvar-delegate` importieren
2. ggf. POM anpassen

## Übung

1. Prozessmodell erstellen das Message sendet

Prozessmodelldatei` /u-processstart-message/src/main/resources/u-send-process.bpmn ` mit Camunda-Modeler öffnen und Prozesselemente erstellen.

![u-processstart-message-screen2](camunda/storage/gruppe-4/u-processstart-message-message/images/2018/09/u-processstart-message-screen2.png)

2. Prozessmodell erstellen das Message empfängt

Prozessmodelldatei `/u-processstart-message/src/main/resources/u-receive-process ` mit Camunda-Modeler öffnen und Prozesselemente erstellen.

![u-processstart-message-screen3](camunda/storage/gruppe-4/u-processstart-message-message/images/2018/09/u-processstart-message-screen3.png)

3. Im Receive-Prozessmodell für das Start-Message-Event definieren, dass eine Message mit dem Namen "SpieltippVorhanden" das Startevent auslöst (d.h. wenn in die Engine eine Message mit diesem Namen reingegeben wird, wir das Start-Event ausgelöst und somit der Prozess gestartet)

![u-processstart-message-screen4](camunda/storage/gruppe-4/u-processstart-message-message/images/2018/09/u-processstart-message-screen4.png)

4. Im Receive-Prozessmodell den Skripttask "Tipp ausgeben" anlegen der die Variable "spieltipp" (die über die Message bei Prozessstart übergeben werden muss) auf der Konsole ausgibt

![u-processstart-message-screen5](camunda/storage/gruppe-4/u-processstart-message-message/images/2018/09/u-processstart-message-screen5.png)

5. Java-Klasse anlegen welche von Send-Task des Send-Prozessmodells aufgerufen wird und eine Message an die Engine übergibt um eine Instanz des Receive-Prozessmodells zu starten.

Dazu im Package `learnbpm.example.processstartmessage` die Java-Klasse `StartProcessTippausgabeDelegate.java` erzeugen die das Interface `org.camunda.bpm.engine.delegate.JavaDelegate` implementiert. Analog zur Übung _u-processvar-delegate_ vorgehen.

```java
// .. TODO ..
```

In der Methode execute erstmal die Prozessvariable `spieltipp` holen. Eine `Map` erzeugen und die Prozessvariable reinstecken. Dann über den `RuntimeService` die Methode `startProcessInstanceByMessage` und den Messagenamen `SpieltippVorhanden` sowie die `Map` übergeben. Damit wird eine Message mit dem Namen und den Variablen in der Map an die Engine übergeben.
```java
@Override
	public void execute(DelegateExecution execution) throws Exception {

		// Prozessvariable "spieltipp" holen
		..TODO..

		//Map mit dem Namen "variables" erstellen und Prozessvariable "spieltipp"
    // in Map stecken. Auf Schreibweise von spieltipp achten!
		..TODO..

		// RuntimeService der Engine holen um auf Prozesse zuzugreifen
		RuntimeService rtm = execution.getProcessEngineServices().getRuntimeService();

	    // Message mit dem dem Namen "SpieltippVorhanden" an die Engine geben um Instanz
		// des Prozessmodells zu starten, das ein Message-Start-Event für den Messagenamen "SpieltippVorhanden"
		// definiert hat.
		rtm.startProcessInstanceByMessage("SpieltippVorhanden",variables);

		// Wichtig, die Map nach Messageerzeugung wieder zu leeren! Vor allem wenn mehrere Prozessinstanzen
		// hintereinander mit Messages gestartet werden sollen.
		variables.clear();

	}
```

Im Prozessmodell `u-send-process.bpmn` den Service-Task mit der Klasse `StartProcessTippausgabeDelegate.java` verknüpfen.

![u-processstart-message-screen6](camunda/storage/gruppe-4/u-processstart-message-message/images/2018/09/u-processstart-message-screen6.png)


5. Alle geänderten Dateien speichern.

## Testen


### Projekt aktualisieren
1. Projekt mit _F5-Taste_ refreshen
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven clean_ um Maven zurückzusetzen
3.  Rechtsklick auf Projektordner  _Maven-> Update Project ..._  dann Projekt anhaken (**Checkboxen beachten! siehe unten**) und _OK_

![general-screen1](camunda/storage/gruppe-4/u-processstart-message-message/images/2018/07/general-screen1.png)

### Test starten

Rechtsklick auf Java-Datei `/u-processstart-message/src/test/java/learnbpm/example/test/processstartmessage/UnitTestProzess.java`  und dann _Run As -> JUnit Test_

Der Test startet eine Instanz des Prozesses u-send-process.bpmn. Dieser läuft durch und der Send-Task "Prozess zur Spieltipp-ausgabe anstossen" stösst über eine Message den Prozess u-receive-process.bpmn an und übergibt in der Message den spieltipp. Der angestossene Prozess gibt den Spieltipp auf der Konsole aus.

**Ergebnis Konsole:**

![u-processstart-message-screen7](camunda/storage/gruppe-4/u-processstart-message-message/images/2018/09/u-processstart-message-screen7.png)

## Testmethoden
Die Testmethode `testProzess` wurde vorab erstellt. Bitte beachten, wie zwei Prozessmodelle in einen JUnit-Test eingebunden werden können.
```java
// JUnit-Test für den gesamten Prozess
	@Test
	@Deployment(resources = { "u-receive-process.bpmn", "u-send-process.bpmn" }) // hier die Prozessmodelle aufführen die getest werden sollen
```

## Projekt deployen (in Camunda-Engine spielen)

1. Projekt aktualisieren (siehe [Projekt aktualisieren](#projekt-aktualisieren))
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven build..._
3. Im Konfigurationsfenster unter Goals `tomcat7:deploy` beim ersten deployen eingeben; ist das Projekt schon mal deployed worden, dann `tomcat7:redeploy` eingeben und mit _OK_ ausführen


## Prozess in camunda starten
1. Im Browser die Tasklist öffnen: `http://localhost:8080/camunda/app/tasklist/default/#/login`
2. Mit Admin einloggen (demo/demo)
3. Rechts oben "Start process" anklicken
4. Prozess im Fenster durch Anklicken auswählen
5. Im Folgefenster roten Knopf "Start" betätigen

## Doku/ weiterführende Quellen

[Abschnitt über Message-Events in Camunda-Doku](https://docs.camunda.org/manual/7.9/reference/bpmn20/events/message-events/#explicitly-triggering-a-message)

## Lösung

Die Lösung finden Sie im gleichnamigen eclipse-Projekt mit der Namensergänzung "-loesung"
