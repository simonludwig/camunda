package learnbpm.example.eformselectbox;

import java.util.HashMap;
import java.util.Map;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.Variables.SerializationDataFormats;

public class SpielelisteErstellenDelegate implements JavaDelegate {

	public SpielelisteErstellenDelegate() {

	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		
		//Erstellen einer leeren Map
		Map<String, String> spieleliste = new HashMap<String, String>();
		
		// Einträge für die Selectbox in Map anlegen
		// erster Parameter der put-Methode (key der Map) wird in Selectbox als Wert hinterlegt
		// zweiter Parameter der put-Methode (value der Map) wird in Selectbox als Texteintrag angezeigt
		// Beispiel: Eintrag in Selectbox "Australien-England"; Wert "AUS-ENG"
		//der ausgewählte Wert kann über die Embedded Form als Prozessvariable weitergegeben werden
		spieleliste.put("AUS-ENG", "Australien-England");
		spieleliste.put("GER-FR", "Deutschland-Frankreich");
		spieleliste.put("ESP-IT", "Spanien-Italien");
		
	
		// Erzeugen einer Prozessvariablen "ALLE_SPIELE" die als Werte die Map mit den Spieleinträgen übergeben bekommt
		// beim Erzeugen werden die Inhalte der Prozessvariablen im JSON-Format abgelegt
		// dies ist notwendig um innerhalb einer Embedded Form über eine Direktive die Einträge
		// für eine Selectbox nutzen zu können
		execution.setVariable("ALLE_SPIELE",Variables.objectValue(spieleliste)
				.serializationDataFormat(SerializationDataFormats.JSON)
				.create());

	}

}
