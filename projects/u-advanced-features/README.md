# u-advanced-features
# Übung "Fortgeschrittene Konzepte: Execution-Listener, Task-Listener, Prozessinstanzen über Code löschen"
Anhand eines vorgefertigten Prozessmodells wird ein Execution-Listener mit zugehöriger Klasse implementiert, der beim Start des Prozesses für diesen automatisch eine Prozessvariable mit Wert erzeugt. Außerdem wird ein Task-Listener samt Klasse implementiert, bei der Erzeugung eines User-Tasks dessen Namen dynamisch um den Wert der Prozessvariable ergänzt. Schließlich wird der Prozess um einen Service-Task ergänzt der die laufende Prozessinstanz löscht.

## Voraussetzungen

1. Eclipse-Projekt `u-advanced-features` importieren
2. ggf. POM anpassen
3. Prozessmodell `/u-advanced-features/src/main/resources/u-advanced-features.bpmn`

![u-advanced-features-screen9](camunda/storage/gruppe-4/u-advanced-featuresfeatures/images/2018/09/u-advanced-features-screen9.png)

## Übung

1. **Execution-Listener erstellen**: Erstellen eines Listeners der auf einen bestimmten Zustand eines Elements einer Prozessinstanz horcht und wenn dieser eintritt eine Java-Instanz erzeugt und eine Methode ausführt. In diesem Fall soll beim Start des Startereignisses eine Prozessvariable `tippername` erzeugt und mit dem Wert Paul belegt werden.

  Eine Klasse mit dem Namen `VariablenBelegenExecutionListenerDelegate.java` im Package `learnbpm.example.advanced `erstellen die das Interface `org.camunda.bpm.engine.delegate.ExecutionListener` implementiert und in deren Methode die Prozessvariable erzeugen, belegen und an die Prozessinstanz zurückgeben.

![u-advanced-features-screen7](camunda/storage/gruppe-4/u-advanced-featuresfeatures/images/2018/09/u-advanced-features-screen7.png)


```java

public class VariablenBelegenExecutionListenerDelegate implements ExecutionListener {

	public VariablenBelegenExecutionListenerDelegate() {

	}

	@Override
	public void notify(DelegateExecution execution) throws Exception {

		// Erzeugen der Prozessvariablen "tippername"und belegen mit Wert
    // analog der Übung  u-processvar-delegate

..TODO..

}
```

Im Prozessmodell einen Execution-Listener für das Startereignis erzeugen und mit der Klasse
   `VariablenBelegenExecutionListenerDelegate.java` verknüpfen.

![u-advanced-features-screen10](camunda/storage/gruppe-4/u-advanced-featuresfeatures/images/2018/09/u-advanced-features-screen10.png)

2. **Task-Listener erstellen:** Erstellen eines Listeners der horcht ob an einem Task ein bestimmter Zustand erreicht ist und bei Eintritt eine Java-Instanz erzeugt und eine Methode ausführt. In diesem Fall soll bei Erzeugung des User-Tasks "Tipp von..." dieser Task geholt und sein Name um den Wert der Prozessvariablen tippername dynamisch ergänzt werden z.B. zu "Tipp von Werner". **Möglicher Verwendungszweck:** So kann der Nutzer mehrere gleiche Taskinstanzen (die sonst immer auch den gleichen Namen haben) in seiner Camunda-Tasklist unterscheiden.

  Eine Klasse mit dem Namen `TaskNameAendernDelegate.java` im Package `learnbpm.example.advanced `erstellen die das Interface `org.camunda.bpm.engine.delegate.TaskListener` implementiert.

![u-advanced-features-screen11](camunda/storage/gruppe-4/u-advanced-featuresfeatures/images/2018/09/u-advanced-features-screen11.png)


  In der entsprechenden Methode soll der tippername geholt und mit der Methode setName des DelegateTask dessen Name dynamisch angepasst werden.

  ```java
  public class TaskNameAendernDelegate implements TaskListener {

  	public TaskNameAendernDelegate() {
    	}
    	@Override
  	public void notify(DelegateTask delegateTask) {

  		// Variable mit dem Namen "tippername" holen, die im Scope des deligierten Tasks
  		// liegen muss; d.h. diese Variable muss der Prozessinstanz zugeordnet sein,
  		// in der sich auch der Delegate Task befindet
  		String my_tippername = (String) delegateTask.getVariable("tippername");

  		//zu Debuggingzwecken
  		System.out.println("Der alte Name des Tasks lautet: "+ delegateTask.getName());

  		// dynamisch den Wert des Tippernamens in den Tasknamen einfügen
  		delegateTask.setName("Tipp von "+my_tippername);

  		//zu Debuggingzwecken
  		System.out.println("Der neue Name des Tasks lautet: "+ delegateTask.getName());
      	}
    }
```

Im Prozessmodell einen Task-Listener für den User-Task "Tipp von ..." erzeugen und mit der Klasse
 `TaskNameAendernDelegate.java` verknüpfen.

![u-advanced-features-screen12](camunda/storage/gruppe-4/u-advanced-featuresfeatures/images/2018/09/u-advanced-features-screen12.png)


3. **Laufende Prozessinstanz löschen**: Über den Zugriff auf die Prozess-Engine soll sich die Prozessinstanz selbst löschen. In diesem Fall soll das Löschen über einen Service-Task mit dem Aufruf der Klasse `learnbpm.example.advanced.ProzessLoeschenDelegate` durchgeführt werden. **Möglicher Verwendungszweck:** Am Anfang eines Prozesses kann abhängig von einer definierten Bedingung das Löschen der Prozessinstanz eingebaut werden, falls verhindert werden soll, dass eine vom Nutzer gestartete Prozessinstanz zum aktuellen Zustand nicht durchgeführt werden soll (in der Camunda-Tasklist kann man die Startbarkeit eines Prozesses nicht vom Zustand anderer Prozess abhängig machen).

Eigenständiges Erzeugen der Javaklasse ProzessLoeschenDelegate analog zur Übung _u-processvar-delegate_.
 ```java
 // .. TODO ..
 ```

In der Methode `execute` der Klasse `ProzessLoeschenDelegate`  Zugriff auf den `RunTimeService` der Engine, holen. Die Id der Prozessinstanz aus der heraus die Klasse aufgerufen wurde holen und löschen dieser Prozessinstanz mit der Methode `deleteProcessInstance` des `RunTimeService`.

```java

// Runtime-Service der laufenden Camunda-Instanz holen
		RuntimeService rtm = execution.getProcessEngineServices().getRuntimeService();

		// Id der Prozessinstanz holen die diese Delegate-Klasse über einen Service-Task
		// aufgerufen hatt
		String pii = execution.getProcessInstanceId();

		//optional: Anzeige der Prozessinstanz-ID zu Debugginzwecken
		System.out.println("ProzessinstanzID: " + pii);

		// Mit dem Runtimeservice Prozessinstanz über deren Id identifizieren
		// und löschen; verpflichtend auch einen Löschungrund angeben
		rtm.deleteProcessInstance(pii, "Löschen der Prozessinstanz zu Testzwecken"+pii);

		//optional: zu Debugginzwecken
		System.out.println("Prozessinstanz "+pii+ " gelöscht");
```

4. Alle geänderten Dateien speichern

## Testen

### Projekt aktualisieren
1. Projekt mit _F5-Taste_ refreshen
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven clean_ um Maven zurückzusetzen
3.  Rechtsklick auf Projektordner  _Maven-> Update Project ..._  dann Projekt anhaken (**Checkboxen beachten! siehe unten**) und _OK_

![general-screen1](camunda/storage/gruppe-4/u-advanced-featuresfeatures/images/2018/07/general-screen1.png)

### Test starten

Rechtsklick auf Java-Datei `/u-advanced-features/src/test/java/learnbpm/example/test/advanced/UnitTestProzess.java`  und dann _Run As -> JUnit Test_

**Ergebnisse Konsole:**

![u-advanced-features-screen13](camunda/storage/gruppe-4/u-advanced-featuresfeatures/images/2018/09/u-advanced-features-screen13.png)

**Info:**
Die Testmethode `testProzess` wurde vorab erstellt. Eine andere Übung beschäftigt sich mit dem Erstellen von Tests.

**HINWEIS**: Um den User-Task zu simulieren muss in der Methode auf diesen zugegriffen werden, was aber diesmal nicht über den Tasknamen möglich ist, da dieser ja dynamisch angepasst wird. Daher kann man die Methode `getTaskDefinitionKey()` verwenden die den Task über die Id der Taskdefinition im Prozessmodell holt (dort heißt sie Id und nicht DefinitionKey).

## Projekt deployen (in Camunda-Engine spielen)

1. Projekt aktualisieren (siehe [Projekt aktualisieren](#projekt-aktualisieren))
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven build..._
3. Im Konfigurationsfenster unter Goals `tomcat7:deploy` beim ersten deployen eingeben; ist das Projekt schon mal deployed worden, dann `tomcat7:redeploy` eingeben und mit _OK_ ausführen


## Prozess in camunda starten
1. Im Browser die Tasklist öffnen: `http://localhost:8080/camunda/app/tasklist/default/#/login`
2. Mit Admin einloggen (demo/demo)
3. Rechts oben "Start process" anklicken
4. Prozess im Fenster durch Anklicken auswählen
5. Im Folgefenster roten Knopf "Start" betätigen


## Prozess durchlaufen

Task "Tipp von ..." unter "All Tasks" auswählen und **claimen**.

![u-advanced-features-screen4](camunda/storage/gruppe-4/u-advanced-featuresfeatures/images/2018/09/u-advanced-features-screen4.png)

Spieltipp eingeben und Task "completen".

![u-advanced-features-screen5](camunda/storage/gruppe-4/u-advanced-featuresfeatures/images/2018/09/u-advanced-features-screen5.png)

**Ergebnis (im Konsolenfenster des Tomcat): **

![u-advanced-features-screen8](camunda/storage/gruppe-4/u-advanced-featuresfeatures/images/2018/09/u-advanced-features-screen8.png)


## Doku/ weiterführende Quellen

[Abschnitt zu Execution-Listener in Camunda-Doku](https://docs.camunda.org/manual/7.8/user-guide/process-engine/delegation-code/#execution-listener)

[Abschnitt Task-Listener in Camunda-Doku](https://docs.camunda.org/manual/7.8/user-guide/process-engine/delegation-code/#task-listener)

[Abschnitt zur Interaktion mit Prozessinstanz in Camunda-Doku](https://docs.camunda.org/manual/7.8/user-guide/process-engine/process-engine-concepts/#interact-with-a-process-instance)


## Lösung

Die Lösung finden Sie im gleichnamigen eclipse-Projekt mit der Namensergänzung "-loesung"
