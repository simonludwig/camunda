# Übung "Service-Task-Delegation mit holen und schreiben von Prozessvariablen"
Es soll ein Prozess erstellt werden, der über einen Skript-Task die beiden Teams eines Fußballspiels festlegt. Über eine Service-Task-Delegation soll dann in einer Java-Klasse die Prozessvariable zum Team geholt und der Wert auf der Konsole ausgegeben werden. Außerdem soll in der Javaklasse eine Prozessvariable mit dem Tipp zu dem Fußballspiel erzeugt mit einem Wert belegt und an den Prozess zurückgegeben werden. Schließlich soll der Spieltipp über einen Skript-Task auf der Konsole ausgegeben werden.

## Voraussetzungen


1. Eclipse-Projekt `u-processvar-delegate` importieren
2. ggf. POM anpassen

## Übung

1. Prozessmodell erstellen
  1. Prozessmodelldatei `/u-processvar-delegate/src/main/resources/u-processvar-delegate.bpmn`  mit Camunda-Modeler öffnen
  2. Prozesselemente erstellen

![u-processvar-delegate-screen1](camunda/storage/gruppe-4/u-processvar-delegatedelegate/images/2018/08/u-processvar-delegate-screen1.png)

2. Skript-Task "Teams für Spieltipp erzeugen" um Skriptcode ergänzen der die Prozessvariable _teams_ erstellt und mit dem Text "Deutschland:Frankreich" belegt.

![u-processvar-delegate-screen2](camunda/storage/gruppe-4/u-processvar-delegatedelegate/images/2018/08/u-processvar-delegate-screen2.png)


3. Java-Delegate-Klasse für Service-Task "Teams ausgeben und Spieltipp erzeugen" erstellen in der die Prozessvariable "teams" geholt und auf der Konsole ausgegeben wird. Außerdem Prozessvariable "spieltipp" erzeugen, mit dem Wert "Tipp: 7:0" belegen und in den Prozess zurückgeben.

  1. Klasse `ErgebnisseTippernameDelegate.java` im Package `learnbpm.example.processvardelegate` anlegen und das Interface `JavaDelegate` implementieren (siehe auch Übung _u-servicetask-delegation_)

```java
public class ErgebnisseTippernameDelegate implements JavaDelegate {

	public ErgebnisseTippernameDelegate() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		// TODO Auto-generated method stub

	}
```

  2. in der execute-Methode Über das execution-Objekt die Methode getVariable nutzen um auf eine Prozessvariable as der Prozessinstanz über deren Namen zuzugreifen (Cast notwendig)
```java
	String teams = (String) execution.getVariable("teams");
  ```

  3. Teams auf der Konsole ausgegeben

  4. Über das execution-Objekt die Methode setVariable nutzen um eine Variable "spieltipp" mit dem Wert "Tipp: 7:0" zu erzeugen und in den Prozess zu geben

```java
		execution.setVariable("spieltipp", "Tipp: 7:0");
  ```

4. Prozessvariablen "teams" und "spieltipp" über einen Skript-Task auf der Konsole ausgeben

![u-processvar-delegate-screen3](camunda/storage/gruppe-4/u-processvar-delegatedelegate/images/2018/08/u-processvar-delegate-screen3.png)


5. Alle geänderten Dateien speichern

## Testen


### Projekt aktualisieren
1. Projekt mit _F5-Taste_ refreshen
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven clean_ um Maven zurückzusetzen
3.  Rechtsklick auf Projektordner  _Maven-> Update Project ..._  dann Projekt anhaken (**Checkboxen beachten! siehe unten**) und _OK_

![general-screen1](camunda/storage/gruppe-4/u-processvar-delegatedelegate/images/2018/07/general-screen1.png)

### Test starten

Rechtsklick auf Java-Datei `/u-processvar-delegate/src/test/java/learnbpm/example/test/processvardelegate/UnitTestProzess.java`  und dann _Run As -> JUnit Test_

**Ergebnisse Konsole:**

![u-processvar-delegate-screen7](camunda/storage/gruppe-4/u-processvar-delegatedelegate/images/2018/08/u-processvar-delegate-screen7.png)

Ergebnisfenster des JUnit-Tests:
![u-processvar-delegate-screen8](camunda/storage/gruppe-4/u-processvar-delegatedelegate/images/2018/08/u-processvar-delegate-screen8.png)

**Info:**
Die Testmethoden `testProzess` und `testTeilprozess` wurden vorab erstellt. Eine andere Übung beschäftigt sich mit dem Erstellen von Tests.

## Projekt deployen (in Camunda-Engine spielen)

1. Projekt aktualisieren (siehe [Projekt aktualisieren](#projekt-aktualisieren))
2. Rechtsklick auf Projektordner und dann  _Run As -> Maven build..._
3. Im Konfigurationsfenster unter Goals `tomcat7:deploy` beim ersten deployen eingeben; ist das Projekt schon mal deployed worden, dann `tomcat7:redeploy` eingeben und mit _OK_ ausführen


## Prozess in camunda starten
1. Im Browser die Tasklist öffnen: `http://localhost:8080/camunda/app/tasklist/default/#/login`
2. Mit Admin einloggen (demo/demo)
3. Rechts oben "Start process" anklicken
4. Prozess im Fenster durch Anklicken auswählen
5. Im Folgefenster roten Knopf "Start" betätigen


## Doku/ weiterführende Quellen

[Abschnitt Prozessvariablen in Camunda Doku](https://docs.camunda.org/manual/7.8/user-guide/process-engine/variables/)


## Lösung

Die Lösung finden Sie im gleichnamigen eclipse-Projekt mit der Namensergänzung "-loesung"
