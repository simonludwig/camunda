package learnbpm.example.twittermessage;


import java.util.Random;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

//Twitter-Bibliothek importieren
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

public class SendeTweetDelegate implements JavaDelegate {
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {

		
		// Hilfsmethoden zum Testen um an jeden Tweet Zufallszahl zu hängen um ihn zu unterscheiden
		// Grund: Twitter meldet Fehler wenn gleicher Tweet zweimal gesendet wird
		Random r = new Random();
		Integer zufallszahl = r.nextInt();
		
		
		// Prozessvariable spielergebnis holen über die execution-Instanz des aktuellen Kontexts (laufender Prozess holen)
				String ergebnis = (String) execution.getVariable("spielergebnis");
				
		//Tweetnachricht zusammenstellen
		String content = zufallszahl.toString()+ " Das Spielergebnis lautet "+ ergebnis;
	    
		// Zugangstoken des Camunda-Twittertestaccounts holen
		AccessToken accessToken = new AccessToken("220324559-jet1dkzhSOeDWdaclI48z5txJRFLCnLOK45qStvo", "B28Ze8VDucBdiE38aVQqTxOyPc7eHunxBVv7XgGim4say");
	   
		//Twitternachricht instanzieren
		Twitter twittermsg = new TwitterFactory().getInstance();
		
		//Authentifizierung bei Twitter
		twittermsg.setOAuthConsumer("lRhS80iIXXQtm6LM03awjvrvk", "gabtxwW8lnSL9yQUNdzAfgBOgIMSRqh7MegQs79GlKVWF36qLS");
		twittermsg.setOAuthAccessToken(accessToken);  
		
		//Senden der Nachricht
		twittermsg.updateStatus(content);
		
		// Tweeterfolg prüfen auf https://twitter.com/camunda_demo

	}

}


