#!/bin/sh
mydir="$(dirname "$BASH_SOURCE")"
cd $mydir
echo 'Camunda wird gestartet. Bitte lassen Sie dieses Fenster die ganze Zeit offen. Zum Beenden Ctrl+C im Terminal drücken'
docker-compose up